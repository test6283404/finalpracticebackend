package com.martin.practice.module.member.bean.vo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "member")
@ApiModel(value = "會員")
public class MemberVO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(value = "會員編號")
	private Integer memberId;
	
	@ApiModelProperty(value = "權限編號")
	private Integer roleId;
	
	@ApiModelProperty(value = "帳號")
	private String account;
	
	@ApiModelProperty(value = "密碼")
	private String password;
	
	@ApiModelProperty(value = "姓名")
	private String memberName;
	
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	@ApiModelProperty(value = "生日")
	private Date memberBirthday;
	
	@ApiModelProperty(value = "自介")
	private String memberInfo;
	
	@ApiModelProperty(value = "照片")
	private byte[] memberPhoto;

	public MemberVO() {
		super();
	}

	public MemberVO(Integer roleId, String account, String password, String memberName, Date memberBirthday,
			String memberInfo, byte[] memberPhoto) {
		super();
		this.roleId = roleId;
		this.account = account;
		this.password = password;
		this.memberName = memberName;
		this.memberBirthday = memberBirthday;
		this.memberInfo = memberInfo;
		this.memberPhoto = memberPhoto;
	}

	public MemberVO(Integer memberId, Integer roleId, String account, String password, String memberName,
			Date memberBirthday, String memberInfo, byte[] memberPhoto) {
		super();
		this.memberId = memberId;
		this.roleId = roleId;
		this.account = account;
		this.password = password;
		this.memberName = memberName;
		this.memberBirthday = memberBirthday;
		this.memberInfo = memberInfo;
		this.memberPhoto = memberPhoto;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public Date getMemberBirthday() {
		return memberBirthday;
	}

	public void setMemberBirthday(Date memberBirthday) {
		this.memberBirthday = memberBirthday;
	}

	public String getMemberInfo() {
		return memberInfo;
	}

	public void setMemberInfo(String memberInfo) {
		this.memberInfo = memberInfo;
	}

	public byte[] getMemberPhoto() {
		return memberPhoto;
	}

	public void setMemberPhoto(byte[] memberPhoto) {
		this.memberPhoto = memberPhoto;
	}

	
}
