package com.martin.practice.module.member.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.martin.practice.module.member.bean.dto.CreateRoleDTO;
import com.martin.practice.module.member.bean.vo.RoleVO;

/**
 * 權限 DAO
 */
@Mapper
public interface RoleDAO {

	/**
	 * 透過roleId尋找Role
	 * 
	 * @param roleId
	 * @return RoleVO
	 */
	@Select("select * from role where roleId=#{roleId}")
	RoleVO findRoleByRoleId(Integer roleId);

	/**
	 * 新增權限
	 * @param roleVO
	 * @return Integer
	 */
	@Insert("insert into role(roleName) values(#{roleName})")
	Integer createRole(CreateRoleDTO createRoleDTO);

}
