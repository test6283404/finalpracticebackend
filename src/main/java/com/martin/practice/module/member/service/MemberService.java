package com.martin.practice.module.member.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.martin.practice.module.member.bean.dto.InputNameDTO;
import com.martin.practice.module.member.bean.vo.MemberVO;
import com.martin.practice.module.member.dao.MemberDAO;

/**
 * 會員模組的Service
 */
@Service
public class MemberService {

	@Autowired
	private MemberDAO memberDAO;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public List<MemberVO> findAllMember(){
		return memberDAO.findAllMember();
	}
	
	/**
	 * 透過memberId尋找Member
	 * @param memberId
	 * @return MemberVO
	 */
	public MemberVO findMemberByMemberId(Integer memberId) {
		return memberDAO.findMemberByMemberId(memberId);
	}
	
	/**
	 * 透過account尋找Member
	 * @param account
	 * @return MemberVO
	 */
	public MemberVO findMemberByAccount(String account) {
		
		try {
			MemberVO memberVO = memberDAO.findMemberByAccouont(account);		
			
			return memberVO;
		} catch (Exception e) {
			System.out.println(e);
			
			return null;
		}
	}
	
	/**
	 * 新增會員
	 * @param memberVO
	 * @return Integer 成功筆數
	 */
	public Integer addMember(MemberVO memberVO) {
		memberVO.setPassword(passwordEncoder.encode(memberVO.getPassword()));
		
		return memberDAO.createMember(memberVO);
	}
	
	/**
	 * 檢查帳戶是否重複
	 * @param account
	 * @return boolean
	 */
	public boolean checkIfAccountIsExist(String account) {
		InputNameDTO inputNameDTO = new InputNameDTO();
		inputNameDTO.setAccount(account);
		
		try {
			MemberVO memberVO = memberDAO.findMemberByAccouont(account);		
			
			return memberVO != null ? true : false;
		} catch (Exception e) {
			System.out.println(e);
			
			return true;
		}
		
	}
	
	/**
	 * 登入檢查
	 * @param account
	 * @param password
	 * @return MemberVO
	 */
	public MemberVO checkLogin(String account, String password) {
		
		try {		
			MemberVO memberVO = memberDAO.findMemberByAccouont(account);
			
			if (passwordEncoder.matches(password, memberVO.getPassword())) {
				return memberVO;
			}
			
			return null;
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 更新Member
	 * @param memberVO
	 * @return Integer 成功筆數
	 */
	public Integer updateMember(MemberVO memberVO) {
		return memberDAO.updateMember(memberVO);
	}
	
	/**
	 * 更新會員密碼
	 * @param memberVO
	 * @return Integer 成功筆數
	 */
	public Integer updateMemberPassword(MemberVO memberVO) {
		memberVO.setPassword(passwordEncoder.encode(memberVO.getPassword()));
		
		return memberDAO.updateMemberPassword(memberVO);
	}
	
	public boolean checkPassword(MemberVO memberVO, String password) {
		if (passwordEncoder.matches(password, memberVO.getPassword())) {
			return true;
		}
		return false;
	}
}
