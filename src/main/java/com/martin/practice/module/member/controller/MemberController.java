package com.martin.practice.module.member.controller;

import static dev.samstevens.totp.util.Utils.getDataUriForImage;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.martin.practice.core.utils.DateUtils;
import com.martin.practice.core.utils.GenerateUtils;
import com.martin.practice.module.member.bean.dto.AccountAndAnimalDTO;
import com.martin.practice.module.member.bean.dto.AccountAndPasswordDTO;
import com.martin.practice.module.member.bean.dto.CreateMemberDTO;
import com.martin.practice.module.member.bean.dto.ForIdDTO;
import com.martin.practice.module.member.bean.dto.LoginMemberDTO;
import com.martin.practice.module.member.bean.dto.UpdateMemberDTO;
import com.martin.practice.module.member.bean.dto.UpdatePasswordDTO;
import com.martin.practice.module.member.bean.vo.MemberVO;
import com.martin.practice.module.member.service.MemberService;
import com.martin.practice.module.mfa.bean.vo.MemberDetailVO;
import com.martin.practice.module.mfa.service.MemberDetailService;

import dev.samstevens.totp.code.HashingAlgorithm;
import dev.samstevens.totp.qr.QrData;
import dev.samstevens.totp.qr.QrGenerator;
import dev.samstevens.totp.qr.ZxingPngQrGenerator;
import dev.samstevens.totp.secret.DefaultSecretGenerator;
import dev.samstevens.totp.secret.SecretGenerator;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 會員模組的Controller
 */
@RestController
public class MemberController {

	@Autowired
	private MemberService memberService;

	@Autowired
	private MemberDetailService memberDetailService;

	Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * Member註冊
	 * 
	 * @param CreateMemberDTO 新增會員的傳入值
	 * @return ResponseEntity<String>
	 * @throws IOException
	 */
	@ApiOperation(value = "Member註冊")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping(value = "/member/create")
	public ResponseEntity<String> createMember(@ModelAttribute CreateMemberDTO createMemberDTO) throws IOException {

		try {
			boolean isExist = memberService.checkIfAccountIsExist(createMemberDTO.getAccount());
			boolean isBirthdayResonable = checkTimeResonable(createMemberDTO.getMemberBirthday());
			boolean checkAccountAndPassword = checkAccontAndPasswordFormat(createMemberDTO.getAccount(),
					createMemberDTO.getPassword());
			boolean checkAnimal = checkAnimalFormat(createMemberDTO.getMemberInfo());
			boolean checkName = checkNameFormat(createMemberDTO.getMemberName());

			System.out.println(createMemberDTO.getMemberName());
			
			if (!checkName) {
				log.error("姓名格式錯誤");

				return new ResponseEntity<>("姓名格式錯誤", HttpStatus.BAD_REQUEST);
			}

			if (!checkAnimal) {
				log.error("動物格式錯誤");

				return new ResponseEntity<>("動物格式錯誤", HttpStatus.BAD_REQUEST);
			}

			if (!checkAccountAndPassword) {
				log.error("帳密格式錯誤");

				return new ResponseEntity<>("帳密格式錯誤", HttpStatus.BAD_REQUEST);
			}

			if (!isBirthdayResonable) {
				log.error("生日不合理");

				return new ResponseEntity<>("生日不合理", HttpStatus.BAD_REQUEST);
			}

			if (isExist) {
				log.error("帳號已被註冊");

				return new ResponseEntity<>("帳號已被註冊", HttpStatus.BAD_REQUEST);
			} else {
//				Part part = createMemberDTO.getMemberPhoto();
//		        BufferedInputStream bufferedInputStream = new BufferedInputStream(part.getInputStream());
//		        byte[] memberPhoto = bufferedInputStream.readAllBytes();
//		        bufferedInputStream.close();

				MemberVO memberVO = new MemberVO(2, createMemberDTO.getAccount(), createMemberDTO.getPassword(),
						createMemberDTO.getMemberName(),
						DateUtils.formatDateFromString(createMemberDTO.getMemberBirthday()),
						createMemberDTO.getMemberInfo(), createMemberDTO.getMemberPhoto().getBytes());

				Integer member = memberService.addMember(memberVO);

				if (member == 0) {
					log.error("帳號註冊錯誤");

					return new ResponseEntity<>("帳號註冊錯誤", HttpStatus.BAD_REQUEST);
				}
				MemberVO memberByAccount = memberService.findMemberByAccount(createMemberDTO.getAccount());

				memberDetailService.createMemberDetail(memberByAccount.getMemberId(), generateKey());

				MemberDetailVO memberDetailByMemberId = memberDetailService
						.findMemberByMemberId(memberByAccount.getMemberId());

				String qRCode = memberDetailService.createQRCode(memberByAccount, memberDetailByMemberId);
				
//				QrData data = new QrData.Builder().label(memberByAccount.getMemberName())
//						.secret(memberDetailByMemberId.getSecretKey()).issuer("java-docker-practice")
//						.algorithm(HashingAlgorithm.SHA1).digits(6).period(30).build();
//
//				QrGenerator generator = new ZxingPngQrGenerator();
//				byte[] imageData = generator.generate(data);
//
//				String mimeType = generator.getImageMimeType();
//
//				String dataUri = getDataUriForImage(imageData, mimeType);

				log.info("註冊成功");

				return new ResponseEntity<String>(qRCode, HttpStatus.OK);
			}

		} catch (Exception e) {
			System.out.println(e);

			log.error("帳號註冊錯誤");

			return new ResponseEntity<>("帳號註冊錯誤", HttpStatus.BAD_REQUEST);
		}

	}

	/**
	 * Member登入
	 * 
	 * @param AccountAndPasswordDTO 登入的傳入值
	 * @param session
	 * @return ResponseEntity<?>
	 */
	@ApiOperation(value = "Member登入")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 401, message = "登入失敗"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/member/login")
	public ResponseEntity<?> login(
			@ApiParam(required = true, value = "Account, Password and Session") @RequestBody AccountAndPasswordDTO accountAndPasswordDTO,
			HttpSession session) {
		MemberVO memberVO = memberService.checkLogin(accountAndPasswordDTO.getAccount(),
				accountAndPasswordDTO.getPassword());

		try {

			if (memberVO != null) {
				LoginMemberDTO loginMember = new LoginMemberDTO(memberVO.getMemberId(), memberVO.getAccount(),
						memberVO.getMemberName(), memberVO.getMemberBirthday(), memberVO.getMemberPhoto(),
						memberVO.getRoleId());

				session.setAttribute("loginMember", loginMember);

				log.info("登入成功");

				return new ResponseEntity<String>("登入成功", HttpStatus.OK);
			}

		} catch (Exception e) {
			System.out.println(e);
			log.error("登入失敗");

			return new ResponseEntity<String>("登入失敗", HttpStatus.UNAUTHORIZED);
		}

		log.error("登入失敗");

		return new ResponseEntity<String>("登入失敗", HttpStatus.UNAUTHORIZED);

	}

	/**
	 * Member更新
	 * 
	 * @param UpdateMemberDTO 更新的傳入值
	 * @return ResponseEntity<String>
	 * @throws IOException
	 * @throws ParseException
	 */
	@ApiOperation(value = "Member更新")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 401, message = "登入失敗"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/member/update")
	public ResponseEntity<String> updateMember(@ModelAttribute UpdateMemberDTO updateMemberDTO)
			throws IOException, ParseException {
		try {
			boolean checkAnimal = checkAnimalFormat(updateMemberDTO.getMemberInfo());
			boolean isBirthdayResonable = checkTimeResonable(updateMemberDTO.getMemberBirthday());
			boolean checkName = checkNameFormat(updateMemberDTO.getMemberName());

			if (!checkName) {
				log.error("姓名格式錯誤");

				return new ResponseEntity<>("姓名格式錯誤", HttpStatus.BAD_REQUEST);
			}
			if (isBirthdayResonable) {
				log.error("生日不合理");

				return new ResponseEntity<>("生日不合理", HttpStatus.BAD_REQUEST);
			}

			if (!checkAnimal) {
				log.error("動物格式錯誤");

				return new ResponseEntity<>("動物格式錯誤", HttpStatus.BAD_REQUEST);
			}

			MemberVO memberVO = memberService.findMemberByMemberId(updateMemberDTO.getMemberId());

			memberVO.setMemberName(updateMemberDTO.getMemberName());
			memberVO.setMemberBirthday(DateUtils.formatDateFromString(updateMemberDTO.getMemberBirthday()));
			memberVO.setMemberInfo(updateMemberDTO.getMemberInfo());
			memberVO.setMemberPhoto(updateMemberDTO.getMemberPhoto().getBytes());

			Integer updateMember = memberService.updateMember(memberVO);

			if (updateMember == 0) {
				log.error("更新失敗");

				return new ResponseEntity<>("更新失敗", HttpStatus.BAD_REQUEST);
			} else {
				log.info("更新成功");

				return new ResponseEntity<String>("更新成功", HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<>("更新失敗", HttpStatus.BAD_REQUEST);
		}

	}

	/**
	 * Member登出
	 * 
	 * @param session
	 * @return ResponseEntity<?>
	 */
	@ApiOperation(value = "Member登出")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 401, message = "登入失敗"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/member/logout")
	public ResponseEntity<?> logout(@ApiParam(required = true, value = "Session only") HttpSession session) {
		LoginMemberDTO loginMemberDTO = (LoginMemberDTO) session.getAttribute("loginMember");

		if (loginMemberDTO != null) {
			memberDetailService.updateMemberDetail(false, loginMemberDTO.getMemberId());
		}
//		memberDetailService.updateMemberDetail(false, 4012);

		session.removeAttribute("loginMember");
		session.removeAttribute("verifyStatus");

		log.info("登出!");

		return new ResponseEntity<String>("登出!", HttpStatus.OK);
	}

	/**
	 * 確認session有沒有存登入訊息
	 * 
	 * @param httpSession
	 * @return ResponseEntity<?>
	 */
	@ApiOperation(value = "確認session有沒有存登入訊息")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 401, message = "登入失敗"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/member/map")
	public ResponseEntity<?> checkSessionValue(
			@ApiParam(required = true, value = "Session only") HttpSession httpSession) {

		log.info("登入檢查");

		LoginMemberDTO loginMemberDTO = (LoginMemberDTO) httpSession.getAttribute("loginMember");

		if (loginMemberDTO == null) {
			log.error("session attribute 空的");
			return new ResponseEntity<String>("session attribute null", HttpStatus.UNAUTHORIZED); // 401
		}

		Map<String, String> responseMap = new HashMap<>();

		responseMap.put("memberId", loginMemberDTO.getMemberId().toString());
		responseMap.put("memberName", loginMemberDTO.getMemberName());

		return new ResponseEntity<Map<String, String>>(responseMap, HttpStatus.OK);
	}

	/**
	 * 找到所有 Member
	 * 
	 * @return ResponseEntity<List<MemberVO>>
	 */
	@ApiOperation(value = "找到所有 Member")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/member/findAll")
	public ResponseEntity<List<MemberVO>> findAllMember() {
		log.info("用 找到所有 Member api ");

		try {
			List<MemberVO> allMember = memberService.findAllMember();

			return new ResponseEntity<>(allMember, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	}

	/**
	 * 透過memberId尋找Member
	 * 
	 * @param memberId
	 * @return ResponseEntity<MemberVO>
	 */
	@ApiOperation(value = "透過memberId尋找Member")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 401, message = "登入失敗"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/member/findById")
	public ResponseEntity<MemberVO> findById(@RequestBody ForIdDTO memberId) {
		log.info("用 透過memberId尋找Member api ");

		try {
			MemberVO memberVO = memberService.findMemberByMemberId(memberId.getId());

			if (memberVO != null) {
				log.info("取得成功");

				return new ResponseEntity<>(memberVO, HttpStatus.OK);
			} else {
				log.error("失敗");

				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 確認有沒有重複登入
	 * 
	 * @param httpSession
	 * @return ResponseEntity<?>
	 */
	@ApiOperation(value = "確認有沒有重複登入")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 401, message = "登入失敗"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@GetMapping("/member/replicateLogin")
	public ResponseEntity<String> replicateLogin(@RequestParam("account") String account, HttpSession httpSession) {

		log.info("重複登入檢查");

		LoginMemberDTO loginMemberDTO = (LoginMemberDTO) httpSession.getAttribute("loginMember");

		if (loginMemberDTO == null) {
			log.error("session attribute 空的");
			return new ResponseEntity<String>("全新登入", HttpStatus.OK);
		}

		MemberVO memberVO = memberService.findMemberByAccount(account);
		MemberDetailVO memberDetailVO = memberDetailService.findMemberByMemberId(memberVO.getMemberId());

		if (memberDetailVO.getLoginStatus()) {
			log.error("重複登入");

//			httpSession.removeAttribute("loginMember");
//			httpSession.removeAttribute("verifyStatus");

			return new ResponseEntity<String>("重複登入", HttpStatus.UNAUTHORIZED);
		}

		return new ResponseEntity<String>("全新登入", HttpStatus.OK);
	}

	/**
	 * 找回密碼
	 * 
	 * @param AccountAndAnimalDTO
	 * @return ResponseEntity<String>
	 */
	@ApiOperation(value = "找回密碼")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 401, message = "登入失敗"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/member/find-your-password")
	public ResponseEntity<String> findYourPassword(@RequestBody AccountAndAnimalDTO accountAndAnimalDTO) {
		log.info("用 找回密碼 api ");

		try {
			MemberVO memberVO = memberService.findMemberByAccount(accountAndAnimalDTO.getAccount());

			if (memberVO != null) {
				if (memberVO.getMemberInfo().equals(accountAndAnimalDTO.getAnimal())) {
					log.info("成功找回密碼");

					String randomPassword = GenerateUtils.getRandomPassword();

					memberVO.setPassword(randomPassword);
					memberService.updateMemberPassword(memberVO);

					return new ResponseEntity<>(randomPassword, HttpStatus.OK);
				}

				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			} else {
				log.error("失敗");

				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 更新密碼
	 * @param updatePasswordDTO
	 * @return
	 */
	@ApiOperation(value = "更新密碼")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 401, message = "登入失敗"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/member/update/password")
	public ResponseEntity<String> updateMemberPassword(@RequestBody UpdatePasswordDTO updatePasswordDTO) {

		try {
			if (!checkPasswordFormat(updatePasswordDTO.getNewPassword())) {
				return new ResponseEntity<>("密碼格式錯誤", HttpStatus.BAD_REQUEST);
			}

			MemberVO memberVO = memberService.findMemberByMemberId(updatePasswordDTO.getMemberId());

			if (memberService.checkPassword(memberVO, updatePasswordDTO.getOldPassword())) {
				memberVO.setPassword(updatePasswordDTO.getNewPassword());

				memberService.updateMemberPassword(memberVO);

				return new ResponseEntity<>("成功更新密碼", HttpStatus.OK);
			}

			return new ResponseEntity<>("更新密碼失敗", HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			return new ResponseEntity<>("更新密碼失敗", HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * 產生密鑰
	 * 
	 * @return String
	 */
	private String generateKey() {
		SecretGenerator secretGenerator = new DefaultSecretGenerator();
		String secret = secretGenerator.generate();

		return secret;
	}

	/**
	 * 整合帳號和密碼的判斷
	 * 
	 * @param account
	 * @param password
	 * @return
	 */
	private boolean checkAccontAndPasswordFormat(String account, String password) {
		return checkAccountFormat(account) && checkPasswordFormat(password);
	}

	/**
	 * 對輸入的帳號進行格式判斷
	 * 
	 * @param account 開頭一個英文大小寫，再加上7個數字
	 * @return
	 */
	private boolean checkAccountFormat(String account) {
		String regex = "^[a-zA-Z]\\d{7}$";
		Pattern pattern = Pattern.compile(regex);

		return pattern.matcher(account).find();
	}

	/**
	 * 對輸入的密碼進行格式判斷
	 * 
	 * @param password 應包含英文大小寫、數字、特殊符號(@, #, $, %)，8~12字
	 * @return
	 */
	private boolean checkPasswordFormat(String password) {
		String regex = "^(?=.*[@#$%])(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z\\d@#$%]{8,12}$";
		Pattern pattern = Pattern.compile(regex);

		return pattern.matcher(password).find();
	}

	/**
	 * 對輸入的生日進行格式判斷
	 * 
	 * @param birthday 不能晚於現在時間出生，不能在1900以前出生
	 * @return
	 * @throws ParseException
	 */
	private boolean checkTimeResonable(String birthday) throws ParseException {
		Date birthdayDate = DateUtils.formatDateFromString(birthday);
		Date date = new Date();

//		System.out.println(DateUtils.validateDate(birthdayDate));
		return birthdayDate.before(date) && DateUtils.validateDate(birthdayDate);
	}

	/**
	 * 對輸入的動物進行格式判斷
	 * 
	 * @param animal
	 * @return
	 */
	private boolean checkAnimalFormat(String animal) {
		String regex = "^(?=.*[^\\d])(?=.*[^@#$%&])[^\\d@#$%&]{1,30}$";

		Pattern pattern = Pattern.compile(regex);

		return pattern.matcher(animal).find();
	}

	/**
	 * 對輸入的姓名進行格式判斷
	 * 
	 * @param name
	 * @return
	 */
	private boolean checkNameFormat(String name) {
		String regex = "^(?=.*[^\\d])(?=.*[^@#$%&])[^\\d@#$%&]{1,25}$";

		Pattern pattern = Pattern.compile(regex);

		return pattern.matcher(name).find();
	}
}
