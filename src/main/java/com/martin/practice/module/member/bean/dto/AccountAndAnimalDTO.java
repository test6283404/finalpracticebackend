package com.martin.practice.module.member.bean.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * 忘記密碼的傳入值
 */
public class AccountAndAnimalDTO {
	
	@ApiModelProperty(value = "帳號")
	private String account;
	
	@ApiModelProperty(value = "喜歡的動物")
	private String animal;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getAnimal() {
		return animal;
	}

	public void setAnimal(String animal) {
		this.animal = animal;
	}
	
}
