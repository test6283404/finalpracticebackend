package com.martin.practice.module.member.bean.dto;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiModelProperty;

/**
 * 用來建立使用者表格
 */
public class CreateMemberDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "帳號")
	private String account;

	@ApiModelProperty(value = "密碼")
	private String password;

	@ApiModelProperty(value = "姓名")
	private String memberName;

	@ApiModelProperty(value = "生日")
	private String memberBirthday;

	@ApiModelProperty(value = "自我簡介")
	private String memberInfo;

	@ApiModelProperty(value = "照片")
	private MultipartFile memberPhoto;

	public CreateMemberDTO() {
		super();
	}

	public CreateMemberDTO(String account, String password, String memberName, String memberBirthday, String memberInfo,
			MultipartFile memberPhoto) {
		super();
		this.account = account;
		this.password = password;
		this.memberName = memberName;
		this.memberBirthday = memberBirthday;
		this.memberInfo = memberInfo;
		this.memberPhoto = memberPhoto;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberBirthday() {
		return memberBirthday;
	}

	public void setMemberBirthday(String memberBirthday) {
		this.memberBirthday = memberBirthday;
	}

	public String getMemberInfo() {
		return memberInfo;
	}

	public void setMemberInfo(String memberInfo) {
		this.memberInfo = memberInfo;
	}

	public MultipartFile getMemberPhoto() {
		return memberPhoto;
	}

	public void setMemberPhoto(MultipartFile memberPhoto) {
		this.memberPhoto = memberPhoto;
	}

	
}
