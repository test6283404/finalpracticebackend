package com.martin.practice.module.member.bean.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * 更新密碼的傳入值
 */
public class UpdatePasswordDTO {
	
	@ApiModelProperty(value = "會員編號")
	private Integer memberId;
	
	@ApiModelProperty(value = "舊密碼")
	private String oldPassword;
	
	@ApiModelProperty(value = "新密碼")
	private String newPassword;

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
}
