package com.martin.practice.module.member.bean.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * 新增權限
 */
public class CreateRoleDTO {
	
	@ApiModelProperty(value = "權限名稱")
	private String roleName;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
}
