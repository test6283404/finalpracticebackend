package com.martin.practice.module.member.bean.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * 傳入帳號
 */
public class InputNameDTO {
	
	@ApiModelProperty(value = "帳號")
	private String account;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}
	
}
