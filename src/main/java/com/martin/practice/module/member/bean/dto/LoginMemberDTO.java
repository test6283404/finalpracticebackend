package com.martin.practice.module.member.bean.dto;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

/**
 * 用來登入
 */
public class LoginMemberDTO {
	
	@ApiModelProperty(value = "會員編號")
	private Integer memberId;

	@ApiModelProperty(value = "帳號")
	private String account;
	
	@ApiModelProperty(value = "會員姓名")
	private String memberName;
	
	@ApiModelProperty(value = "會員生日")
	private Date memberBirthday;
	
	@ApiModelProperty(value = "會員照片")
	private byte[] memberPhoto;

	@ApiModelProperty(value = "會員權限")
	private Integer roleId;

	public LoginMemberDTO() {
		super();
	}

	public LoginMemberDTO(Integer memberId, String account, String memberName, Date memberBirthday, byte[] memberPhoto,
			Integer roleId) {
		super();
		this.memberId = memberId;
		this.account = account;
		this.memberName = memberName;
		this.memberBirthday = memberBirthday;
		this.memberPhoto = memberPhoto;
		this.roleId = roleId;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public Date getMemberBirthday() {
		return memberBirthday;
	}

	public void setMemberBirthday(Date memberBirthday) {
		this.memberBirthday = memberBirthday;
	}

	public byte[] getMemberPhoto() {
		return memberPhoto;
	}

	public void setMemberPhoto(byte[] memberPhoto) {
		this.memberPhoto = memberPhoto;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	
}
