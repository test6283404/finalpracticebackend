package com.martin.practice.module.member.bean.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * 只傳入帳號和密碼
 */
public class AccountAndPasswordDTO {
	
	@ApiModelProperty(value = "帳號")
	private String account;
	
	@ApiModelProperty(value = "密碼")
	private String password;

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
