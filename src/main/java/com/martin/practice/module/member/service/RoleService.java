package com.martin.practice.module.member.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.martin.practice.module.member.bean.dto.CreateRoleDTO;
import com.martin.practice.module.member.dao.RoleDAO;

/**
 * 權限模組的Service
 */
@Service
public class RoleService {
	
	@Autowired
	private RoleDAO roleDAO;
	
	/**
	 * 新增Role
	 * @param CreateRoleDTO
	 * @return Integer 成功筆數
	 */
	public Integer addRole(CreateRoleDTO createRoleDTO) {
		return roleDAO.createRole(createRoleDTO);
	}
}
