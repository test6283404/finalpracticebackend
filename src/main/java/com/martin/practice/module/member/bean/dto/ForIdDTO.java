package com.martin.practice.module.member.bean.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * 傳入各式各樣編號
 */
public class ForIdDTO {
	
	@ApiModelProperty(value = "編號")
	private Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	
}
