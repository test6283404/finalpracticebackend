package com.martin.practice.module.member.bean.dto;

import java.io.Serializable;

import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.ApiModelProperty;

/**
 * 用來更新使用者的資訊，不含帳密
 */
public class UpdateMemberDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "會員編號")
	private Integer memberId;
	
	@ApiModelProperty(value = "姓名")
	private String memberName;

	@ApiModelProperty(value = "生日")
	private String memberBirthday;

	@ApiModelProperty(value = "自我簡介")
	private String memberInfo;

	@ApiModelProperty(value = "照片")
	private MultipartFile memberPhoto;

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberBirthday() {
		return memberBirthday;
	}

	public void setMemberBirthday(String memberBirthday) {
		this.memberBirthday = memberBirthday;
	}

	public String getMemberInfo() {
		return memberInfo;
	}

	public void setMemberInfo(String memberInfo) {
		this.memberInfo = memberInfo;
	}

	public MultipartFile getMemberPhoto() {
		return memberPhoto;
	}

	public void setMemberPhoto(MultipartFile memberPhoto) {
		this.memberPhoto = memberPhoto;
	}
    
}
