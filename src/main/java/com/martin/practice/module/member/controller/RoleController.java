package com.martin.practice.module.member.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.martin.practice.module.member.bean.dto.CreateRoleDTO;
import com.martin.practice.module.member.service.RoleService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 權限模組Controller
 */
@RestController
public class RoleController {
	
	@Autowired
	private RoleService roleService;
	
	Logger log = LoggerFactory.getLogger(getClass());
	
	/**
	 * 新增Role
	 * @param CreateRoleDTO 權限的傳入值
	 * @return ResponseEntity<String>
	 */
	@ApiOperation(value = "新增Role")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/role/create")
	public ResponseEntity<String> createRole(@RequestBody CreateRoleDTO createRoleDTO) {
		Integer role = roleService.addRole(createRoleDTO);
		
		if (role == 0) {
			log.error("權限新增失敗");

			return new ResponseEntity<>("權限新增失敗", HttpStatus.BAD_REQUEST);
		} else {
			log.info("權限新增成功");

			return new ResponseEntity<String>("權限新增成功", HttpStatus.OK);
		}
	}
}
