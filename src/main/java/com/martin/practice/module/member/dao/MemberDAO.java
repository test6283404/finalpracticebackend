package com.martin.practice.module.member.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.martin.practice.module.member.bean.vo.MemberVO;

/**
 * 會員 DAO
 * 
 * @author D3012203
 */
@Mapper
public interface MemberDAO {

	/**
	 * 取得所有Member
	 * 
	 * @return List<MemberVO>
	 */
	@Select("select * from member")
	List<MemberVO> findAllMember();

	/**
	 * 透過memberId尋找Member
	 * 
	 * @param memberId
	 * @return MemberVO
	 */
	@Select("select * from member where memberId=#{memberId}")
	MemberVO findMemberByMemberId(Integer memberId);

	@Select("select * from member where account=#{account}")
	MemberVO findMemberByAccouont(String account);
	
	/**
	 * 新增會員
	 * 
	 * @param memberVO
	 * @return Integer
	 */
	@Insert("insert into member(roleId, account, password, memberName, memberBirthday, memberInfo, memberPhoto) "
			+ " values(#{roleId}, #{account}, #{password}, #{memberName}"
			+ ", #{memberBirthday}, #{memberInfo}, #{memberPhoto})")
	Integer createMember(MemberVO memberVO);

	/**
	 * 更新Member
	 * 
	 * @param memberVO
	 * @return Integer
	 */
	@Update("update member set roleId=#{roleId}"
			+ ", memberName=#{memberName}, memberBirthday=#{memberBirthday}"
			+ ", memberInfo=#{memberInfo}, memberPhoto=#{memberPhoto} where memberId=#{memberId}")
	Integer updateMember(MemberVO memberVO);
	
	/**
	 * 更新會員密碼
	 * 
	 * @param memberVO
	 * @return Integer
	 */
	@Update("update member set password=#{password} where memberId=#{memberId}")
	Integer updateMemberPassword(MemberVO memberVO);
}
