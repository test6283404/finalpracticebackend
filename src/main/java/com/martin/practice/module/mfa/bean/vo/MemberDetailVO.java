package com.martin.practice.module.mfa.bean.vo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "memberDetail")
@ApiModel(value = "會員細節")
public class MemberDetailVO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(value = "會員細節編號")
	private Integer memberDetailId;
	
	@ApiModelProperty(value = "會員編號")
	private Integer memberId;
	
	@ApiModelProperty(value = "密鑰")
	private String secretKey;
	
	@ApiModelProperty(value = "登入狀態")
	private boolean loginStatus;

	public Integer getMemberDetailId() {
		return memberDetailId;
	}

	public void setMemberDetailId(Integer memberDetailId) {
		this.memberDetailId = memberDetailId;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public boolean getLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(boolean loginStatus) {
		this.loginStatus = loginStatus;
	}
}
