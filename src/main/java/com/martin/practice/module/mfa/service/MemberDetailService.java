package com.martin.practice.module.mfa.service;

import static dev.samstevens.totp.util.Utils.getDataUriForImage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.martin.practice.module.member.bean.vo.MemberVO;
import com.martin.practice.module.mfa.bean.vo.MemberDetailVO;
import com.martin.practice.module.mfa.dao.MemberDetailDAO;

import dev.samstevens.totp.code.HashingAlgorithm;
import dev.samstevens.totp.exceptions.QrGenerationException;
import dev.samstevens.totp.qr.QrData;
import dev.samstevens.totp.qr.QrGenerator;
import dev.samstevens.totp.qr.ZxingPngQrGenerator;

/**
 * 多驗證模組的Service
 */
@Service
public class MemberDetailService {
	
	@Autowired
	private MemberDetailDAO memberDetailDAO;
	
	/**
	 * 透過memberId尋找Member
	 * @param memberId
	 * @return MemberDetailVO
	 */
	public MemberDetailVO findMemberByMemberId(Integer memberId) {
		return memberDetailDAO.findMemberDetailByMemberId(memberId);
	}
	
	/**
	 * 新增會員細節
	 * @param memberDetailVO
	 * @return Integer 成功筆數
	 */
	public Integer createMemberDetail(Integer memberId, String secretKey) {
		return memberDetailDAO.createMemberDetail(memberId, secretKey, false);
	}
	
	/**
	 * 更新會員細節
	 * @param loginStatus
	 * @return Integer
	 */
	public Integer updateMemberDetail(boolean loginStatus, Integer memberId) {
		return memberDetailDAO.updateMemberDetail(loginStatus, memberId);
	}
	
	/**
	 * 生成攜帶密鑰的QRCode
	 * @param memberVO 提供姓名
	 * @param memberDetailVO 提供密鑰
	 * @return QRCode字串
	 * @throws QrGenerationException
	 */
	public String createQRCode(MemberVO memberVO, MemberDetailVO memberDetailVO) throws QrGenerationException {
		QrData qrData = new QrData.Builder().label(memberVO.getMemberName())
				.secret(memberDetailVO.getSecretKey()).issuer("java-docker-practice")
				.algorithm(HashingAlgorithm.SHA1).digits(6).period(30).build();

		QrGenerator generator = new ZxingPngQrGenerator();
		byte[] imageData = generator.generate(qrData);

		String mimeType = generator.getImageMimeType();

		return getDataUriForImage(imageData, mimeType);
		
	}
}
