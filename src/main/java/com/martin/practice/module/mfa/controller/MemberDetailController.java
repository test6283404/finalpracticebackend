package com.martin.practice.module.mfa.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.martin.practice.module.member.bean.dto.LoginMemberDTO;
import com.martin.practice.module.mfa.bean.dto.VerifyMemberDTO;
import com.martin.practice.module.mfa.bean.vo.MemberDetailVO;
import com.martin.practice.module.mfa.service.MemberDetailService;

import dev.samstevens.totp.code.CodeVerifier;
import dev.samstevens.totp.code.DefaultCodeGenerator;
import dev.samstevens.totp.code.DefaultCodeVerifier;
import dev.samstevens.totp.time.SystemTimeProvider;
import dev.samstevens.totp.time.TimeProvider;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 多重驗證模組的Controller
 */
@RestController
public class MemberDetailController {

	@Autowired
	private MemberDetailService memberDetailService;

	Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * 驗證 OTP密碼
	 * 
	 * @param code
	 * @param httpSession
	 * @return ResponseEntity<String>
	 */
	@GetMapping("/verify")
	private ResponseEntity<String> verify(@RequestParam("code") String code, HttpSession httpSession) {
		TimeProvider timeProvider = new SystemTimeProvider();
		DefaultCodeGenerator codeGenerator = new DefaultCodeGenerator();
		CodeVerifier verifier = new DefaultCodeVerifier(codeGenerator, timeProvider);

		LoginMemberDTO loginMemberDTO = (LoginMemberDTO) httpSession.getAttribute("loginMember");
		MemberDetailVO memberDetailVO = memberDetailService.findMemberByMemberId(loginMemberDTO.getMemberId());

		boolean successful = verifier.isValidCode(memberDetailVO.getSecretKey(), code);

		if (successful) {
			memberDetailService.updateMemberDetail(true, memberDetailVO.getMemberId());

			VerifyMemberDTO verifyMemberDTO = new VerifyMemberDTO();
			verifyMemberDTO.setMemberId(memberDetailVO.getMemberId());
			verifyMemberDTO.setLoginStatus(successful);
			
			httpSession.setAttribute("verifyStatus", verifyMemberDTO);

			return new ResponseEntity<String>("驗證成功", HttpStatus.OK);
		}
//		httpSession.removeAttribute("loginMember");
//		httpSession.removeAttribute("verifyStatus");

		return new ResponseEntity<>("驗證失敗", HttpStatus.BAD_REQUEST);
	}

	/**
	 * 確認session有沒有存驗證訊息
	 * 
	 * @param httpSession
	 * @return ResponseEntity<?>
	 */
	@ApiOperation(value = "確認session有沒有存驗證訊息")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 401, message = "登入失敗"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/verify/map")
	public ResponseEntity<?> testSessionValue(
			@ApiParam(required = true, value = "Session only") HttpSession httpSession) {

		log.info("驗證檢查");

		VerifyMemberDTO verifyStatus = (VerifyMemberDTO) httpSession.getAttribute("verifyStatus");

		if (verifyStatus == null) {
			log.error("session attribute 空的");
			return new ResponseEntity<String>("session attribute null", HttpStatus.UNAUTHORIZED); // 401
		}

		Map<String, Integer> responseMap = new HashMap<>();

		responseMap.put("memberId", verifyStatus.getMemberId());

		return new ResponseEntity<Map<String, Integer>>(responseMap, HttpStatus.OK);
	}
}
