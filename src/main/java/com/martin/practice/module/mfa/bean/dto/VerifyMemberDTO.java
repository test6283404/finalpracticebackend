package com.martin.practice.module.mfa.bean.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * 傳入會員編號和登入狀態
 */
public class VerifyMemberDTO {
	
	@ApiModelProperty(value = "會員編號")
	private Integer memberId;
	
	@ApiModelProperty(value = "登入狀態")
	private boolean loginStatus;

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public boolean isLoginStatus() {
		return loginStatus;
	}

	public void setLoginStatus(boolean loginStatus) {
		this.loginStatus = loginStatus;
	}
	
}
