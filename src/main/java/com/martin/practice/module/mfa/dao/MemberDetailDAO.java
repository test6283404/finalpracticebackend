package com.martin.practice.module.mfa.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.martin.practice.module.mfa.bean.vo.MemberDetailVO;

/**
 * 多驗證DAO
 */
@Mapper
public interface MemberDetailDAO {
	
	/**
	 * 透過memberId尋找memberDetail
	 * 
	 * @param memberId
	 * @return MemberDetailVO
	 */
	@Select("select * from memberDetail where memberId=#{memberId}")
	MemberDetailVO findMemberDetailByMemberId(Integer memberId);
	
	/**
	 * 新增會員細節
	 * 
	 * @param memberDetailVO
	 * @return Integer
	 */
	@Insert("insert into memberDetail(memberId, secretKey, loginStatus)"
			+ " values(#{memberId}, #{secretKey}, #{loginStatus})")
	Integer createMemberDetail(Integer memberId, String secretKey, boolean loginStatus);
	
	/**
	 * 更新會員細節
	 * @param loginStatus
	 * @return Integer
	 */
	@Update("update memberDetail set loginStatus=#{loginStatus} where memberId=#{memberId}")
	Integer updateMemberDetail(boolean loginStatus, Integer memberId); 
}
