package com.martin.practice.module.activity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.martin.practice.module.activity.bean.dto.CreateGroupDTO;
import com.martin.practice.module.activity.dao.GroupDAO;

/**
 * 暫時不用
 */
@Service
public class GroupService {
	
	@Autowired
	private GroupDAO groupDAO;
	
	/**
	 * 透過activityDetailId尋找memberId
	 * @param activityDetailId
	 * @return List<Integer>
	 */
	public List<Integer> findgroupMemberIdByactivityDetailId(Integer activityDetailId){
		return groupDAO.findgroupMemberIdByactivityDetailId(activityDetailId);
	}
	
	/**
	 * 新增group
	 * @param groupVO
	 * @return Integer
	 */
	public Integer createGroup(CreateGroupDTO createGroupDTO) {
		return groupDAO.createGroup(createGroupDTO);
	}
}
