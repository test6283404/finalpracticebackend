package com.martin.practice.module.activity.bean.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * 新增活動傳入值
 */
public class CreateActivityDTO {
	
	@ApiModelProperty(value = "活動名")
	private String activityName;

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	
}
