package com.martin.practice.module.activity.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.martin.practice.module.activity.bean.dto.CreateDiscussDTO;
import com.martin.practice.module.activity.bean.vo.DiscussVO;
import com.martin.practice.module.activity.service.DiscussService;
import com.martin.practice.module.member.bean.dto.ForIdDTO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 活動模組controller-評論
 */
@RestController
public class DiscussController {

	@Autowired
	private DiscussService discussService;
	
	Logger log = LoggerFactory.getLogger(getClass());
	
	/**
	 * 透過activityDetailId尋找Discuss
	 * 
	 * @param activityDetailId
	 * @return ResponseEntity<List<DiscussVO>>
	 */
	@ApiOperation(value = "透過活動細節編號尋找Discuss")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/discuss/findById")
	public ResponseEntity<List<DiscussVO>> findById(@ApiParam(required = true, value = "請傳入活動細節編號") @RequestBody ForIdDTO activityDetailId) {
		log.info("用 透過活動細節編號尋找Discuss api ");

		try {
			List<DiscussVO> discussList = discussService.findDiscussByActivityDetailId(activityDetailId.getId());

			if (!discussList.isEmpty()) {
				log.info("取得成功");

				return new ResponseEntity<>(discussList, HttpStatus.OK);
			} else {
				log.error("失敗");

				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 透過memberId尋找Discuss
	 * 
	 * @param memberId
	 * @return ResponseEntity<List<DiscussVO>>
	 */
	@ApiOperation(value = "透過會員編號尋找Discuss")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/discuss/findByMemberId")
	public ResponseEntity<List<DiscussVO>> findByMemberId(@ApiParam(required = true, value = "請傳入活動細節編號") @RequestBody ForIdDTO memberId) {
		log.info("用 透過會員編號尋找Discuss api ");

		try {
			List<DiscussVO> discussList = discussService.findDiscussByMemberId(memberId.getId());

			if (!discussList.isEmpty()) {
				log.info("取得成功");

				return new ResponseEntity<>(discussList, HttpStatus.OK);
			} else {
				log.error("失敗");

				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 新增評論
	 * @param CreateDiscussDTO
	 * @return ResponseEntity<String>
	 */
	@ApiOperation(value = "新增評論")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/discuss/create")
	public ResponseEntity<String> create(
			@ApiParam(required = true,value = "新增評論") @RequestBody CreateDiscussDTO createDiscussDTO) {
		log.info("用 新增評論 api ");
		
		try {
			if (createDiscussDTO.getDiscussContent() != "") {
				Integer create = discussService.createDiscuss(createDiscussDTO);
				
				if (create == 1) {
					log.info("新增評論成功");
					
					return new ResponseEntity<>("新增評論成功", HttpStatus.OK);
				} else {
					log.error("評論新增失敗");
					
					return new ResponseEntity<>("評論新增失敗", HttpStatus.NOT_FOUND);
				}
			}
			
			return new ResponseEntity<>("評論新增失敗", HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			log.error("評論新增失敗");
			
			return new ResponseEntity<>("評論新增失敗", HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 刪除評論
	 * @param ForIdDTO
	 * @return ResponseEntity<String>
	 */
	@ApiOperation(value = "刪除評論")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/discuss/delete")
	public ResponseEntity<String> delete(
			@ApiParam(required = true,value = "刪除評論") @RequestBody ForIdDTO discussId) {
		log.info("用 刪除評論 api ");
		
		try {
			discussService.deleteDiscuss(discussId.getId());

			log.info("評論刪除成功");
			
			return new ResponseEntity<>("評論刪除成功", HttpStatus.OK);
		} catch (Exception e) {
			log.error("評論刪除失敗");
			
			return new ResponseEntity<>("評論刪除失敗", HttpStatus.BAD_REQUEST);
		}
	}
}
