package com.martin.practice.module.activity.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.martin.practice.module.activity.bean.vo.ActivityDetailVO;

/**
 * 活動細節DAO
 */
@Mapper
public interface ActivityDetailDAO {
	
	/**
	 * 取得所有ActivityDetail
	 * @return List<ActivityDetailVO>
	 */
	@Select("select * from activityDetail")
	List<ActivityDetailVO> findAllActivityDetail();
	
	/**
	 * 透過activityDetailId尋找ActivityDetail
	 * @param activityDetailId
	 * @return ActivityDetailVO
	 */
	@Select("select * from activityDetail where activityDetailId=#{activityDetailId}")
	ActivityDetailVO findActivityDetailByActivityDetailId(Integer activityDetailId);
	
	/**
	 * 透過activityId尋找ActivityDetail
	 * @param activityId
	 * @return ActivityDetailVO
	 */
	@Select("select * from activityDetail where activityId=#{activityId}")
	List<ActivityDetailVO> findActivityDetailByActivityId(Integer activityId);
	
	/**
	 * 新增ActivityDetail
	 * @param CreateActivityDetailDTO
	 * @return Integer
	 */
	@Insert("insert into activityDetail(activityId, memberId, activityDetailName"
			+ ", activityDetailInfo, startDate, endDate) "
			+ "values(#{activityId}, #{memberId}, #{activityDetailName}"
			+ ", #{activityDetailInfo}, #{startDate}, #{endDate})")
	Integer createActivityDetail(ActivityDetailVO activityDetailVO);
	
	/**
	 * 更新ActivityDetail
	 * @param activityDetailVO
	 * @return Integer
	 */
	@Update("update activityDetail set activityId=#{activityId}, memberId=#{memberId}, activityDetailName=#{activityDetailName}"
			+ ", activityDetailInfo=#{activityDetailInfo}, startDate=#{startDate}, endDate=#{endDate}")
	Integer updatActivityDetail(ActivityDetailVO activityDetailVO);
	
	/**
	 * 刪除ActivityDetail
	 * @param activityDetailId
	 * @return Integer
	 */
	@Delete("delete from activityDetail where activityDetailId=#{activityDetailId}")
	Integer deleteActivityDetail(Integer activityDetailId);

}
