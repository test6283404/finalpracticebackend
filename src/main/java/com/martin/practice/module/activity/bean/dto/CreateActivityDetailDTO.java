package com.martin.practice.module.activity.bean.dto;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

/**
 * 活動細節的傳入值
 */
public class CreateActivityDetailDTO {

//	@NotNull(message = "沒有活動編號")
	@ApiModelProperty(value = "活動編號")
	private Integer activityId;

	@ApiModelProperty(value = "會員編號")
	private Integer memberId;

	@ApiModelProperty(value = "活動細節名稱")
	private String activityDetailName;

	@ApiModelProperty(value = "活動細節介紹")
	private String activityDetailInfo;

//	@FutureOrPresent(message = "時間不能是過去")
	@ApiModelProperty(value = "活動細節開始時間")
	private Date startDate;

//	@FutureOrPresent(message = "時間不能是過去")
	@ApiModelProperty(value = "活動細節結束時間")
	private Date endDate;

	public Integer getActivityId() {
		return activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getActivityDetailName() {
		return activityDetailName;
	}

	public void setActivityDetailName(String activityDetailName) {
		this.activityDetailName = activityDetailName;
	}

	public String getActivityDetailInfo() {
		return activityDetailInfo;
	}

	public void setActivityDetailInfo(String activityDetailInfo) {
		this.activityDetailInfo = activityDetailInfo;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

}
