package com.martin.practice.module.activity.bean.vo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "discuss")
@ApiModel(value = "評論")
public class DiscussVO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(value = "評論編號")
	private Integer discussId;
	
	@ApiModelProperty(value = "會員編號")
	private Integer memberId;
	
	@ApiModelProperty(value = "活動細節編號")
	private Integer activityDetailId;
	
	@ApiModelProperty(value = "評論內容")
	private String discussContent;
	
//    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
//	@Temporal(TemporalType.TIMESTAMP)
	@ApiModelProperty(value = "評論時間")
	private Date discussCreatedAt;

	public Integer getDiscussId() {
		return discussId;
	}

	public void setDiscussId(Integer discussId) {
		this.discussId = discussId;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public Integer getActivityDetailId() {
		return activityDetailId;
	}

	public void setActivityDetailId(Integer activityDetailId) {
		this.activityDetailId = activityDetailId;
	}

	public String getDiscussContent() {
		return discussContent;
	}

	public void setDiscussContent(String discussContent) {
		this.discussContent = discussContent;
	}

	public Date getDiscussCreatedAt() {
		return discussCreatedAt;
	}

	public void setDiscussCreatedAt(Date discussCreatedAt) {
		this.discussCreatedAt = discussCreatedAt;
	}
	
	@PrePersist
    public void onCreate() {
        if (discussCreatedAt == null) {
        	discussCreatedAt = new Date();
        }
    }
}
