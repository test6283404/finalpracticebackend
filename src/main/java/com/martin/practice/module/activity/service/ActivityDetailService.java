package com.martin.practice.module.activity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.martin.practice.module.activity.bean.vo.ActivityDetailVO;
import com.martin.practice.module.activity.dao.ActivityDetailDAO;

/**
 * 活動細節Service
 */
@Service
public class ActivityDetailService {

	@Autowired
	private ActivityDetailDAO activityDetailDAO;
	
	/**
	 * 取得所有ActivityDetail
	 * @return List<ActivityDetailVO>
	 */
	public List<ActivityDetailVO> findAllActivityDetail(){
		return activityDetailDAO.findAllActivityDetail();
	}
	
	/**
	 * 透過activityDetailId尋找ActivityDetail
	 * @param activityDetailId
	 * @return ActivityDetailVO
	 */
	public ActivityDetailVO findActivityDetailByActivityDetailId(Integer activityDetailId) {
		return activityDetailDAO.findActivityDetailByActivityDetailId(activityDetailId);
	}
	
	/**
	 * 透過activityId尋找ActivityDetail
	 * @param activityId
	 * @return ActivityDetailVO
	 */
	public List<ActivityDetailVO> findActivityDetailByActivityId(Integer activityId) {
		return activityDetailDAO.findActivityDetailByActivityId(activityId);
	}
	
	/**
	 * 新增ActivityDetail
	 * @param CreateActivityDetailDTO
	 * @return Integer
	 */
	public Integer createActivityDetail(ActivityDetailVO activityDetailVO) {
		return activityDetailDAO.createActivityDetail(activityDetailVO);
	}
	
	/**
	 * 更新ActivityDetail
	 * @param activityDetailVO
	 * @return Integer
	 */
	public Integer updatActivityDetail(ActivityDetailVO activityDetailVO) {
		return activityDetailDAO.updatActivityDetail(activityDetailVO);
	}
	
	/**
	 * 刪除ActivityDetail
	 * @param activityDetailId
	 * @return Integer
	 */
	public Integer deleteActivityDetail(Integer activityDetailId) {
		return activityDetailDAO.deleteActivityDetail(activityDetailId);
	}
}
