package com.martin.practice.module.activity.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.martin.practice.module.activity.bean.vo.DiscussVO;

/**
 * 評論DAO
 */
@Mapper
public interface DiscussDAO {
	
	/**
	 * 透過活動取得評論
	 * @param activityDetailId
	 * @return
	 */
	@Select("select * from discuss where activityDetailId=#{activityDetailId}")
	List<DiscussVO> findDiscussByActivityDetailId(Integer activityDetailId);
	
	/**
	 * 透過會員取得評論
	 * @param memberId
	 * @return
	 */
	@Select("select * from discuss where memberId=#{memberId}")
	List<DiscussVO> findDiscussByMemberId(Integer memberId);
	
	/**
	 * 新增評論
	 * @param discussVO
	 * @return
	 */
	@Insert("insert into discuss(memberId, activityDetailId, discussContent, discussCreatedAt) "
			+ "values(#{memberId}, #{activityDetailId}, #{discussContent}, #{discussCreatedAt})")
	Integer createDiscuss(DiscussVO discussVO);
	
	/**
	 * 刪除評論
	 * @param discussId
	 * @return
	 */
	@Delete("delete from discuss where discussId=#{discussId}")
	Integer deleteDiscuss(Integer discussId);
}
