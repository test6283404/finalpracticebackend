package com.martin.practice.module.activity.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.martin.practice.module.activity.bean.dto.CreateActivityDTO;
import com.martin.practice.module.activity.bean.dto.UpdateActivityDTO;
import com.martin.practice.module.activity.bean.vo.ActivityVO;
import com.martin.practice.module.activity.service.ActivityService;
import com.martin.practice.module.member.bean.dto.ForIdDTO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 活動模組controller
 */
@RestController
public class ActivityController {
	
	@Autowired
	private ActivityService activityService;
	
	Logger log = LoggerFactory.getLogger(getClass()); 
	
	/**
	 * 找到所有活動
	 * @return ResponseEntity<List<ActivityVO>>
	 */
	@ApiOperation(value = "找到所有活動")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/activity/findAll")
	public ResponseEntity<List<ActivityVO>> findAll() {
		log.info("用 取得所有活動資料 api ");
		
		try {
			List<ActivityVO> activityList = activityService.findAllActivity();

			if (!activityList.isEmpty()) {
				log.info("活動取得成功");
				
				return new ResponseEntity<>(activityList, HttpStatus.OK);
			} else {
				log.error("活動取得失敗");
				
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("活動取得失敗");
			
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 透過activityId尋找Activity
	 * 
	 * @param id
	 * @return Assets
	 */
	@ApiOperation(value = "取得特定id的資料")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/activity/findById")
	public ResponseEntity<ActivityVO> findById(@ApiParam(required = true, value = "請傳入活動編號") @RequestBody ForIdDTO activityId) {
		log.info("用 取得特定id的資料 api ");

		try {
			ActivityVO activityVO = activityService.findAllActivityByActivityId(activityId.getId());

			if (activityVO != null) {
				log.info("取得成功");

				return new ResponseEntity<>(activityVO, HttpStatus.OK);
			} else {
				log.error("失敗");

				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 新增活動
	 * @param CreateActivityDTO
	 * @return ResponseEntity<String>
	 */
	@ApiOperation(value = "新增活動")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/activity/create")
	public ResponseEntity<String> create(
			@ApiParam(required = true,value = "新增活動") @RequestBody CreateActivityDTO createActivityDTO) {
		log.info("用 新增活動 api ");
		
		try {
			if (createActivityDTO.getActivityName() != "") {
				Integer create = activityService.createActivity(createActivityDTO);
				
				if (create == 1) {
					log.info("新增活動成功");
					
					return new ResponseEntity<>("新增活動成功", HttpStatus.OK);
				} else {
					log.error("活動新增失敗");
					
					return new ResponseEntity<>("活動新增失敗", HttpStatus.NOT_FOUND);
				}
			}
			
			return new ResponseEntity<>("活動新增失敗", HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			log.error("活動新增失敗");
			
			return new ResponseEntity<>("活動新增失敗", HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Activity更新
	 * @param memberVO
	 * @return ResponseEntity<String>
	 */
	@ApiOperation(value = "Activity更新")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 401, message = "登入失敗"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/activity/update")
	public ResponseEntity<String> updateActivity(@RequestBody UpdateActivityDTO updateActivityDTO) {
		log.info("用 Activity更新 api ");
		
		ActivityVO activityVO = activityService.findAllActivityByActivityId(updateActivityDTO.getActivityId());
		
		activityVO.setActivityName(updateActivityDTO.getActivityName());

		Integer updateActivity = activityService.updateActivity(activityVO);
		
		if (updateActivity == 0) {
			log.error("活動更新失敗");

			return new ResponseEntity<>("活動更新失敗", HttpStatus.BAD_REQUEST);
		} else {		
			log.info("活動更新成功");

			return new ResponseEntity<String>("活動更新成功", HttpStatus.OK);
		}
	}
	
	/**
	 * 刪除活動
	 * @param UpdateActivityDTO
	 * @return ResponseEntity<String>
	 */
	@ApiOperation(value = "刪除活動")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/activity/delete")
	public ResponseEntity<String> delete(
			@ApiParam(required = true,value = "刪除活動") @RequestBody ForIdDTO updateActivityDTO) {
		log.info("用 刪除活動 api ");
		
		try {
			activityService.deleteActivity(updateActivityDTO.getId());

			log.info("活動刪除成功");
			
			return new ResponseEntity<>("活動刪除成功", HttpStatus.OK);
		} catch (Exception e) {
			log.error("活動刪除失敗");
			
			return new ResponseEntity<>("活動刪除失敗", HttpStatus.BAD_REQUEST);
		}
	}
}
