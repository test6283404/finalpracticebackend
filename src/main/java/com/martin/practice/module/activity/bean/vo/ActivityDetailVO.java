package com.martin.practice.module.activity.bean.vo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "activityDetail")
@ApiModel(value = "活動細節")
public class ActivityDetailVO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(value = "活動細節編號")
	private Integer activityDetailId;
	
	@ApiModelProperty(value = "活動細節編號")
	private Integer activityId;
	
	@ApiModelProperty(value = "會員編號")
	private Integer memberId;
	
	@ApiModelProperty(value = "活動細節名稱")
	private String activityDetailName;
	
	@ApiModelProperty(value = "活動細節資訊")
	private String activityDetailInfo;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@ApiModelProperty(value = "活動細節開始時間")
	private Date startDate;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@ApiModelProperty(value = "活動細節結束時間")
	private Date endDate;

	
	public ActivityDetailVO(Integer activityId, Integer memberId, String activityDetailName, String activityDetailInfo,
			Date startDate, Date endDate) {
		super();
		this.activityId = activityId;
		this.memberId = memberId;
		this.activityDetailName = activityDetailName;
		this.activityDetailInfo = activityDetailInfo;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public ActivityDetailVO(String activityDetailName, String activityDetailInfo, Date startDate, Date endDate) {
		super();
		this.activityDetailName = activityDetailName;
		this.activityDetailInfo = activityDetailInfo;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public ActivityDetailVO() {
		super();
	}

	public Integer getActivityDetailId() {
		return activityDetailId;
	}

	public void setActivityDetailId(Integer activityDetailId) {
		this.activityDetailId = activityDetailId;
	}

	public Integer getActivityId() {
		return activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getActivityDetailName() {
		return activityDetailName;
	}

	public void setActivityDetailName(String activityDetailName) {
		this.activityDetailName = activityDetailName;
	}

	public String getActivityDetailInfo() {
		return activityDetailInfo;
	}

	public void setActivityDetailInfo(String activityDetailInfo) {
		this.activityDetailInfo = activityDetailInfo;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
}
