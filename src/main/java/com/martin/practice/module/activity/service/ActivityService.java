package com.martin.practice.module.activity.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.martin.practice.module.activity.bean.dto.CreateActivityDTO;
import com.martin.practice.module.activity.bean.vo.ActivityVO;
import com.martin.practice.module.activity.dao.ActivityDAO;

/**
 * 活動Service
 */
@Service
public class ActivityService {

	@Autowired
	private ActivityDAO activityDAO;

	/**
	 * 取得所有Activity
	 * @return List<ActivityVO>
	 */
	public List<ActivityVO> findAllActivity() {
		return activityDAO.findAllActivity();
	}

	/**
	 * 透過activityId尋找Activity
	 * @param activityId
	 * @return ActivityVO
	 */
	public ActivityVO findAllActivityByActivityId(Integer activityId) {
		return activityDAO.findAllActivityByActivityId(activityId);
	}

	/**
	 * 新增Activity
	 * @param createActivityDTO
	 * @return Integer 成功筆數
	 */
	public Integer createActivity(CreateActivityDTO createActivityDTO) {
		ActivityVO activityVO = activityDAO.findActivityByActivityName(createActivityDTO.getActivityName());

		if (activityVO != null) {
			return 0;
		} else {
			return activityDAO.createActivity(createActivityDTO);
		}
	}

	/**
	 * 更新Activity
	 * @param activityVO
	 * @return Integer 成功筆數
	 */
	public Integer updateActivity(ActivityVO activityVO) {
		return activityDAO.updateActivity(activityVO);
	}

	/**
	 * 刪除Activity
	 * @param activityId
	 * @return Integer 成功筆數
	 */
	public Integer deleteActivity(Integer activityId) {
		return activityDAO.deleteActivity(activityId);
	}
}
