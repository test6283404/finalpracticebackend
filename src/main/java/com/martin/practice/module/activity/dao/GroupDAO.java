package com.martin.practice.module.activity.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.martin.practice.module.activity.bean.dto.CreateGroupDTO;

/**
 * 暫時不用
 */
@Mapper
public interface GroupDAO {
	
	/**
	 * 透過activityDetailId尋找memberId
	 * @param activityDetailId
	 * @return List
	 */
	@Select("select g.groupMemberId from [group] g where activityDetailId=#{activityDetailId}")
	List<Integer> findgroupMemberIdByactivityDetailId(Integer activityDetailId);
	
	/**
	 * 新增group
	 * @param groupVO
	 * @return List
	 */
	@Insert("insert into [group](activityDetailId, groupMemberId) values (#{activityDetailId}, #{groupMemberId})")
	Integer createGroup(CreateGroupDTO createGroupDTO);
}
