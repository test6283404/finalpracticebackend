package com.martin.practice.module.activity.bean.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * 更新活動的傳入值
 */
public class UpdateActivityDTO {
	
	@ApiModelProperty(value = "活動編號")
	private Integer activityId;
	
	@ApiModelProperty(value = "活動名")
	private String activityName;

	public Integer getActivityId() {
		return activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}
	
}
