package com.martin.practice.module.activity.bean.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * 評論的傳入值
 */
public class CreateDiscussDTO {
	
	@ApiModelProperty(value = "會員編號")
	private Integer memberId;
	
	@ApiModelProperty(value = "活動細節編號")
	private Integer activityDetailId;
	
	@ApiModelProperty(value = "討論內容")
	private String discussContent;

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public Integer getActivityDetailId() {
		return activityDetailId;
	}

	public void setActivityDetailId(Integer activityDetailId) {
		this.activityDetailId = activityDetailId;
	}

	public String getDiscussContent() {
		return discussContent;
	}

	public void setDiscussContent(String discussContent) {
		this.discussContent = discussContent;
	}
	
}
