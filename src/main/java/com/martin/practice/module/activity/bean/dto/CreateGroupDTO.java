package com.martin.practice.module.activity.bean.dto;

/**
 * 暫時不用
 */
public class CreateGroupDTO {
	private Integer activityDetailId;
	
	private Integer groupMemberId;

	public Integer getActivityDetailId() {
		return activityDetailId;
	}

	public void setActivityDetailId(Integer activityDetailId) {
		this.activityDetailId = activityDetailId;
	}

	public Integer getGroupMemberId() {
		return groupMemberId;
	}

	public void setGroupMemberId(Integer groupMemberId) {
		this.groupMemberId = groupMemberId;
	}
	
}
