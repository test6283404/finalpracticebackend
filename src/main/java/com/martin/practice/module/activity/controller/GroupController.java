package com.martin.practice.module.activity.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.martin.practice.module.activity.bean.dto.CreateGroupDTO;
import com.martin.practice.module.activity.bean.vo.ActivityDetailVO;
import com.martin.practice.module.activity.service.ActivityDetailService;
import com.martin.practice.module.activity.service.GroupService;
import com.martin.practice.module.member.bean.dto.ForIdDTO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 暫時不用
 */
@RestController
public class GroupController {

	@Autowired
	private GroupService groupService;

	@Autowired
	private ActivityDetailService activityDetailService;

	Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * 透過activityDetailId尋找groupMember
	 * 
	 * @param ForIdDTO
	 * @return ResponseEntity<List<?>>
	 */
	@ApiOperation(value = "透過activityDetailId尋找groupMember")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/group/findByActivityDetailId")
	public ResponseEntity<List<?>> findById(
			@ApiParam(required = true, value = "請傳入活動編號") @RequestBody ForIdDTO activityDetailId) {
		log.info("用 透過activityDetailId尋找groupMember api ");

		try {
			ActivityDetailVO activityDetailVO = activityDetailService
					.findActivityDetailByActivityDetailId(activityDetailId.getId());

			if (activityDetailVO != null) {
				log.info("取得成功");

				List<Integer> memberList = groupService.findgroupMemberIdByactivityDetailId(activityDetailId.getId());
								
				return new ResponseEntity<>(memberList, HttpStatus.OK);
			} else {
				log.error("失敗");

				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("失敗");

			System.out.println(e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 新增活動團隊
	 * 
	 * @param CreateActivityDTO
	 * @return ResponseEntity<String>
	 */
	@ApiOperation(value = "新增活動團隊")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/group/create")
	public ResponseEntity<String> create(
			@ApiParam(required = true, value = "新增活動團隊") @RequestBody CreateGroupDTO createGroupDTO) {
		log.info("用 新增活動團隊 api ");

		try {
			Integer create = groupService.createGroup(createGroupDTO);

			if (create == 1) {
				log.info("新增活動團隊成功");

				return new ResponseEntity<>("新增活動團隊成功", HttpStatus.OK);
			}
			
			return new ResponseEntity<>("活動團隊失敗", HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			log.error("活動團隊新增失敗");

			return new ResponseEntity<>("活動團隊新增失敗", HttpStatus.BAD_REQUEST);
		}
	}
}
