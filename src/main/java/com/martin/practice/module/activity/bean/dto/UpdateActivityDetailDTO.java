package com.martin.practice.module.activity.bean.dto;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

/**
 * 更新活動細節的傳入值
 */
public class UpdateActivityDetailDTO {
	
	@ApiModelProperty(value = "活動細節編號")
	private Integer activityDetailId;
	
	@ApiModelProperty(value = "活動細節名")
	private String activityDetailName;
	
	@ApiModelProperty(value = "活動細節簡介")
	private String activityDetailInfo;
	
	@ApiModelProperty(value = "開始時間")
	private Date startDate;
	
	@ApiModelProperty(value = "結束時間")
	private Date endDate;

	public Integer getActivityDetailId() {
		return activityDetailId;
	}

	public void setActivityDetailId(Integer activityDetailId) {
		this.activityDetailId = activityDetailId;
	}

	public String getActivityDetailName() {
		return activityDetailName;
	}

	public void setActivityDetailName(String activityDetailName) {
		this.activityDetailName = activityDetailName;
	}

	public String getActivityDetailInfo() {
		return activityDetailInfo;
	}

	public void setActivityDetailInfo(String activityDetailInfo) {
		this.activityDetailInfo = activityDetailInfo;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
}
