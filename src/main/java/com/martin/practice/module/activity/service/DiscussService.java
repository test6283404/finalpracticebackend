package com.martin.practice.module.activity.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.martin.practice.module.activity.bean.dto.CreateDiscussDTO;
import com.martin.practice.module.activity.bean.vo.DiscussVO;
import com.martin.practice.module.activity.dao.DiscussDAO;

/**
 * 評論Service
 */
@Service
public class DiscussService {

	@Autowired
	private DiscussDAO discussDAO;
	
	/**
	 * 透過activityDetailId尋找Discuss
	 * @param activityDetailId
	 * @return List<DiscussVO>
	 */
	public List<DiscussVO> findDiscussByActivityDetailId(Integer activityDetailId){
		return discussDAO.findDiscussByActivityDetailId(activityDetailId);
	}
	
	/**
	 * 透過memberId尋找Discuss
	 * @param memberId
	 * @return List<DiscussVO>
	 */
	public List<DiscussVO> findDiscussByMemberId(Integer memberId){
		return discussDAO.findDiscussByMemberId(memberId);
	}
	
	/**
	 * 新增Discuss
	 * @param CreateDiscussDTO
	 * @return Integer
	 */
	public Integer createDiscuss(CreateDiscussDTO createDiscussDTO) {
		DiscussVO discussVO = new DiscussVO();
		
		discussVO.setMemberId(createDiscussDTO.getMemberId());
		discussVO.setActivityDetailId(createDiscussDTO.getActivityDetailId());
		discussVO.setDiscussContent(createDiscussDTO.getDiscussContent());
		discussVO.setDiscussCreatedAt(new Date());
		
		return discussDAO.createDiscuss(discussVO);
	}
	
	/**
	 * 刪除Discuss
	 * @param discussId
	 * @return Integer
	 */
	public Integer deleteDiscuss(Integer discussId) {
		return discussDAO.deleteDiscuss(discussId);
	}
}
