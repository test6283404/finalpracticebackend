package com.martin.practice.module.activity.controller;

import java.util.List;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.martin.practice.core.utils.DateUtils;
import com.martin.practice.module.activity.bean.dto.CreateActivityDetailDTO;
import com.martin.practice.module.activity.bean.dto.UpdateActivityDetailDTO;
import com.martin.practice.module.activity.bean.vo.ActivityDetailVO;
import com.martin.practice.module.activity.service.ActivityDetailService;
import com.martin.practice.module.member.bean.dto.ForIdDTO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 活動模組controller-細節
 */
@RestController
public class ActivityDetailController {

	@Autowired
	private ActivityDetailService activityDetailService;

	Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * 取得所有活動細節
	 * 
	 * @return ResponseEntity<List<Assets>>
	 */
	@ApiOperation(value = "取得所有活動細節")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/activityDetail/findAll")
	public ResponseEntity<List<ActivityDetailVO>> findAll() {
		log.info("用 取得所有活動細節 api ");

		try {
			List<ActivityDetailVO> activityDetailList = activityDetailService.findAllActivityDetail();

			if (!activityDetailList.isEmpty()) {
				log.info("取得成功");

				return new ResponseEntity<>(activityDetailList, HttpStatus.OK);
			} else {
				log.error("失敗");

				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 透過activityDetailId尋找ActivityDetail
	 * 
	 * @param activityDetailId
	 * @return ResponseEntity<ActivityDetailVO>
	 */
	@ApiOperation(value = "取得活動細節id的資料")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/activityDetail/findById")
	public ResponseEntity<ActivityDetailVO> findById(
			@ApiParam(required = true, value = "請傳入活動細節編號") @RequestBody ForIdDTO activityDetailId) {
		log.info("用 取得活動細節id的資料 api ");

		try {
			ActivityDetailVO activityDetailVO = activityDetailService
					.findActivityDetailByActivityDetailId(activityDetailId.getId());

			if (activityDetailVO != null) {
				log.info("取得成功");

				return new ResponseEntity<>(activityDetailVO, HttpStatus.OK);
			} else {
				log.error("失敗");

				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 透過activityId尋找ActivityDetail
	 * 
	 * @param activityId
	 * @return ResponseEntity<ActivityDetailVO>
	 */
	@ApiOperation(value = "取得活動細節的資料")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/activityDetail/findByActivityId")
	public ResponseEntity<List<ActivityDetailVO>> findActivityDetailByActivityId(
			@ApiParam(required = true, value = "請傳入活動編號") @RequestBody ForIdDTO activityId) {
		log.info("用 取得活動細節的資料 api ");

		try {
			List<ActivityDetailVO> activityDetailVO = activityDetailService
					.findActivityDetailByActivityId(activityId.getId());

			if (activityDetailVO != null) {
				log.info("取得成功");

				return new ResponseEntity<>(activityDetailVO, HttpStatus.OK);
			} else {
				log.error("失敗");

				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 新增活動細節
	 * 
	 * @param CreateActivityDetailDTO
	 * @return ResponseEntity<String>
	 */
	@ApiOperation(value = "新增活動細節")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/activityDetail/create")
	public ResponseEntity<String> create(
			@ApiParam(required = true, value = "新增活動細節") @RequestBody CreateActivityDetailDTO createActivityDetailDTO) {
		log.info("用 新增活動細節 api ");

		try {
			if (createActivityDetailDTO.getActivityId() == 0) {
				return new ResponseEntity<>("活動細節新增失敗", HttpStatus.BAD_REQUEST);
			}

			if (createActivityDetailDTO.getActivityDetailName() != ""
					&& createActivityDetailDTO.getActivityDetailInfo() != "") {
				if (!validateName(createActivityDetailDTO.getActivityDetailName())) {
					return new ResponseEntity<>("活動名格式錯誤", HttpStatus.BAD_REQUEST);
				}

				if (!validateDescription(createActivityDetailDTO.getActivityDetailInfo())) {
					return new ResponseEntity<>("活動簡介格式錯誤", HttpStatus.BAD_REQUEST);
				}

				if (!DateUtils.validateDate(createActivityDetailDTO.getStartDate())) {
					return new ResponseEntity<>("活動時間格式錯誤", HttpStatus.BAD_REQUEST);
				}

				if (!DateUtils.validateDate(createActivityDetailDTO.getEndDate())) {
					return new ResponseEntity<>("活動時間格式錯誤", HttpStatus.BAD_REQUEST);
				}

				if (!DateUtils.checkTimeResonable(createActivityDetailDTO.getStartDate(),
						createActivityDetailDTO.getEndDate())
						&& DateUtils.validateDate(createActivityDetailDTO.getStartDate())
						&& DateUtils.validateDate(createActivityDetailDTO.getEndDate())) {
					return new ResponseEntity<>("活動時間不合理", HttpStatus.BAD_REQUEST);
				}

				ActivityDetailVO activityDetailVO = new ActivityDetailVO(createActivityDetailDTO.getActivityId(),
						createActivityDetailDTO.getMemberId(), createActivityDetailDTO.getActivityDetailName(),
						createActivityDetailDTO.getActivityDetailInfo(), createActivityDetailDTO.getStartDate(),
						createActivityDetailDTO.getEndDate());

				Integer create = activityDetailService.createActivityDetail(activityDetailVO);

				if (create == 1) {
					log.info("新增活動細節成功");

					return new ResponseEntity<>("新增活動細節成功", HttpStatus.OK);
				} else {
					log.error("活動細節新增失敗");

					return new ResponseEntity<>("活動細節新增失敗", HttpStatus.NOT_FOUND);
				}
			}

			return new ResponseEntity<>("活動細節新增失敗", HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			log.error("活動細節新增失敗");

			System.out.println(e);
			return new ResponseEntity<>("活動細節新增失敗", HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 更新活動細節
	 * 
	 * @param UpdateActivityDetailDTO
	 * @return ResponseEntity<String>
	 */
	@ApiOperation(value = "更新活動細節")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 401, message = "登入失敗"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/activityDetail/update")
	public ResponseEntity<String> updateActivity(@RequestBody UpdateActivityDetailDTO updateActivityDetailDTO) {
		log.info("用 更新活動細節 api ");

		ActivityDetailVO activityDetailVO = activityDetailService
				.findActivityDetailByActivityDetailId(updateActivityDetailDTO.getActivityDetailId());

		activityDetailVO.setActivityDetailName(updateActivityDetailDTO.getActivityDetailName());
		activityDetailVO.setActivityDetailInfo(updateActivityDetailDTO.getActivityDetailInfo());
		activityDetailVO.setStartDate(updateActivityDetailDTO.getStartDate());
		activityDetailVO.setEndDate(updateActivityDetailDTO.getEndDate());

		Integer updateActivityDetail = activityDetailService.updatActivityDetail(activityDetailVO);

		if (updateActivityDetail == 0) {
			log.error("活動更新失敗");

			return new ResponseEntity<>("活動更新失敗", HttpStatus.BAD_REQUEST);
		} else {
			log.info("活動更新成功");

			return new ResponseEntity<String>("活動更新成功", HttpStatus.OK);
		}
	}

	/**
	 * 刪除活動細節
	 * 
	 * @param ForIdDTO
	 * @return ResponseEntity<String>
	 */
	@ApiOperation(value = "刪除活動細節")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/activityDetail/delete")
	public ResponseEntity<String> delete(
			@ApiParam(required = true, value = "刪除活動細節") @RequestBody ForIdDTO activityDetailId) {
		log.info("用 刪除活動細節 api ");

		try {
			activityDetailService.deleteActivityDetail(activityDetailId.getId());

			log.info("活動細節刪除成功");

			return new ResponseEntity<>("活動細節刪除成功", HttpStatus.OK);
		} catch (Exception e) {
			log.error("活動細節刪除失敗");

			return new ResponseEntity<>("活動細節刪除失敗", HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 對輸入的活動名進行格式判斷
	 * 
	 * @param name
	 * @return
	 */
	private boolean validateName(String name) {
		String regex = "^(?=.*[^&])[^&]{1,20}$";

		Pattern pattern = Pattern.compile(regex);

		return pattern.matcher(name).find();
	}

	/**
	 * 對輸入的活動簡介進行格式判斷
	 * 
	 * @param name
	 * @return
	 */
	private boolean validateDescription(String description) {
		String regex = "^(?=.*[^&])[^&]{1,100}$";

		Pattern pattern = Pattern.compile(regex);

		return pattern.matcher(description).find();
	}
}
