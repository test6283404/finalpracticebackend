package com.martin.practice.module.activity.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.martin.practice.module.activity.bean.dto.CreateActivityDTO;
import com.martin.practice.module.activity.bean.vo.ActivityVO;

/**
 * 活動DAO
 */
@Mapper
public interface ActivityDAO {

	/**
	 * 取得所有Activity
	 * 
	 * @return List<ActivityVO>
	 */
	@Select("select * from activity")
	List<ActivityVO> findAllActivity();

	/**
	 * 透過activityId尋找Activity
	 * 
	 * @param activityId
	 * @return ActivityVO
	 */
	@Select("select * from activity where activityId=#{activity}")
	ActivityVO findAllActivityByActivityId(Integer activityId);

	/**
	 * 透過activityName尋找Activity
	 * 
	 * @param activityName
	 * @return ActivityVO
	 */
	@Select("select * from activity where activityName=#{activityName}")
	ActivityVO findActivityByActivityName(String activityName);
	
	/**
	 * 新增Activity
	 * 
	 * @param activityVO
	 * @return
	 */
	@Insert("insert into activity(activityName) values(#{activityName})")
	Integer createActivity(CreateActivityDTO createActivityDTO);

	/**
	 * 更新Activity
	 * 
	 * @param activityVO
	 * @return Integer
	 */
	@Update("update activity set activityName=#{activityName} where activityId=#{activityId}")
	Integer updateActivity(ActivityVO activityVO);

	/**
	 * 刪除Activity
	 * 
	 * @param activityId
	 * @return Integer
	 */
	@Delete("delete from activity where activityId=#{activityId}")
	Integer deleteActivity(Integer activityId);
}
