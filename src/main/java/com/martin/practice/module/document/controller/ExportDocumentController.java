package com.martin.practice.module.document.controller;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.martin.practice.module.document.service.ExportDocumentService;
import com.martin.practice.module.member.bean.dto.ForIdDTO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 文件模組Controller
 */
@RestController
public class ExportDocumentController {
	
	@Autowired
	private ExportDocumentService exportDocumentService;
	
	Logger log = LoggerFactory.getLogger(getClass());
	
	/**
	 * 生產excel表格，內容是特定會員的對話內容和留言時間
	 * @param memberId
	 * @return
	 */
	@ApiOperation(value = "生產excel表格")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/excel/create")
	public ResponseEntity<String> create(
			@ApiParam(required = true, value = "新增訊息") @RequestBody ForIdDTO memberId, HttpServletResponse httpServletResponse) {
		log.info("用 生產excel表格 api ");

		try {
			log.info("excel表格新增成功");
			
			exportDocumentService.createExcel(memberId.getId(), httpServletResponse);
			
			return new ResponseEntity<>("excel表格新增成功", HttpStatus.OK); 
		} catch (Exception e) {
			log.error("excel表格新增失敗");

			System.out.println(e);
			return new ResponseEntity<>("excel表格新增失敗", HttpStatus.BAD_REQUEST);
		}
	}
}
