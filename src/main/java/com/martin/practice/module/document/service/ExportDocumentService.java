package com.martin.practice.module.document.service;

import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.martin.practice.core.utils.DateUtils;
import com.martin.practice.module.chat.bean.vo.MessageVO;
import com.martin.practice.module.chat.service.MessageService;
import com.martin.practice.module.member.bean.vo.MemberVO;
import com.martin.practice.module.member.service.MemberService;

/**
 * 文件模組Service
 */
@Service
public class ExportDocumentService {

	@Autowired
	private MessageService messageService;

	@Autowired
	private MemberService memberService;

	/**
	 * 透過會員編號取得會員聊天紀錄，並轉成Excel檔案
	 * 
	 * @param memberId
	 * @throws IOException
	 * @throws ParseException
	 */
	public void createExcel(Integer memberId, HttpServletResponse httpServletResponse) throws IOException, ParseException {
		// 計數器，紀錄list數量，並包含欄位名稱
		int counter = 1;

		List<MessageVO> messageList = messageService.findMessageByMemberId(memberId);

		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("會員聊天紀錄");

		// 存放list資料
		Map<String, Object[]> data = new TreeMap<>();

		data.put("1", new Object[] { "聊天內容", "聊天時間" });

		for (MessageVO messageVO : messageList) {
			data.put(Integer.toString(counter + 1),
					new Object[] { messageVO.getMessageContent(), messageVO.getMessageCreatedAt() });

			counter++;
		}

		// 包含欄位名稱，總共有幾個欄位
		Set<String> keySet = data.keySet();
		int rowNumber = 0;

		for (String key : keySet) {
			Row row = sheet.createRow(rowNumber++);

			Object[] objectArray = data.get(key);

			int cellNumber = 0;

			// 欄位填寫
			for (Object object : objectArray) {
				Cell cell = row.createCell(cellNumber++);

				if (object instanceof String) {
					cell.setCellValue((String) object);
				} else if (object instanceof Date) {
					cell.setCellValue(DateUtils.transformDateToString((Date) object));
				}
			}
		}

		MemberVO memberVO = memberService.findMemberByMemberId(memberId);
		String memberName = memberVO.getMemberName();

//		FileOutputStream fileOutputStream = new FileOutputStream(
//				"C:\\Users\\林泰均\\Desktop\\docker-compose-practice\\會員聊天紀錄-" + memberName + ".xlsx");
//		workbook.write(fileOutputStream);
		
		OutputStream outputStream = null;
		
		try {
			String filename = new String(memberName.getBytes());			
			httpServletResponse.setContentType("application/vnd.ms-excel;charset=utf-8");
			httpServletResponse.addHeader("Content-Disposition", "attachment;filename=" + filename + ".xlsx");
			httpServletResponse.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
			outputStream = httpServletResponse.getOutputStream();
			
			workbook.write(outputStream);
		} catch (Exception e) {
			outputStream.close();
			workbook.close();
		}

//		fileOutputStream.close();
//		workbook.close();
	}
}
