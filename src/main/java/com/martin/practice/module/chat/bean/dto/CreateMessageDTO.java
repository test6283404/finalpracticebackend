package com.martin.practice.module.chat.bean.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * 傳入會員編號和訊息
 */
public class CreateMessageDTO {
	
	@ApiModelProperty(value = "會員編號")
	private Integer memberId;

	@ApiModelProperty(value = "聊天內容")
	private String messageContent;

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

}
