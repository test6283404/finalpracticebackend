package com.martin.practice.module.chat.dao;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.martin.practice.module.chat.bean.vo.MessageVO;

/**
 * 訊息DAO
 */
@Mapper
public interface MessageDAO {
	
	/**
	 * 透過chatId尋找Message
	 * @param List<MessageVO>
	 * @return
	 */
	@Select("select * from [message] where chatId=#{chatId}")
	List<MessageVO> findMessageByChatId(Integer chatId);
	
	/**
	 * 透過memberId尋找Message
	 * @param memberId
	 * @return List<MessageVO>
	 */
	@Select("select * from [message] where memberId=#{memberId}")
	List<MessageVO> findMessageByMemberId(Integer memberId);
	
	/**
	 * 新增Message
	 * @param messageVO
	 * @return
	 */
	@Insert("insert into [message](chatId, memberId, messageContent, messageCreatedAt)"
			+ " values(#{chatId}, #{memberId}, #{messageContent}, #{messageCreatedAt})")
	Integer createMessage(MessageVO messageVO);

}
