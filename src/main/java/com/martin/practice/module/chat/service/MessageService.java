package com.martin.practice.module.chat.service;

import java.util.*;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.martin.practice.module.chat.bean.dto.CreateMessageDTO;
import com.martin.practice.module.chat.bean.vo.MessageVO;
import com.martin.practice.module.chat.dao.MessageDAO;

/**
 * 訊息Service
 * 
 */
@Service
public class MessageService {
	
	@Autowired
	private MessageDAO messageDAO;
	
	/**
	 * 透過chatId尋找訊息
	 * @param chatId
	 * @return List<MessageVO>
	 */
	public List<MessageVO> findMessageByChatId(Integer chatId){
		return messageDAO.findMessageByChatId(chatId);
	}
	
	/**
	 * 透過memberId尋找訊息
	 * @param memberId
	 * @return List<MessageVO>
	 */
	public List<MessageVO> findMessageByMemberId(Integer memberId){
		return messageDAO.findMessageByMemberId(memberId);
	}
	
	/**
	 * 新增訊息
	 * @param createMessageDTO
	 * @return Integer
	 */
	public Integer createMessage(CreateMessageDTO createMessageDTO) {
		MessageVO messageVO = new MessageVO();
		
		messageVO.setMemberId(createMessageDTO.getMemberId());
		messageVO.setMessageContent(createMessageDTO.getMessageContent());
		messageVO.setMessageCreatedAt(new Date());
		
		return messageDAO.createMessage(messageVO);
	}
}
