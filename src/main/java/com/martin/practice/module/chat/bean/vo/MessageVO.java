package com.martin.practice.module.chat.bean.vo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "message")
@ApiModel(value = "訊息")
public class MessageVO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(value = "訊息編號")
	private Integer messageId;
	
	@ApiModelProperty(value = "聊天室編號")
	private Integer chatId;
	
	@ApiModelProperty(value = "會員編號")
	private Integer memberId;
	
	@ApiModelProperty(value = "訊息內容")
	private String messageContent;
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@ApiModelProperty(value = "訊息建立時間")
	private Date messageCreatedAt;

	public Integer getMessageId() {
		return messageId;
	}

	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}

	public Integer getChatId() {
		return chatId;
	}

	public void setChatId(Integer chatId) {
		this.chatId = chatId;
	}

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(String messageContent) {
		this.messageContent = messageContent;
	}

	public Date getMessageCreatedAt() {
		return messageCreatedAt;
	}

	public void setMessageCreatedAt(Date messageCreatedAt) {
		this.messageCreatedAt = messageCreatedAt;
	}
	
}
