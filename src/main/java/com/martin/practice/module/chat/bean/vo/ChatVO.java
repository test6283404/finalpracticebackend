package com.martin.practice.module.chat.bean.vo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "chat")
@ApiModel(value = "聊天")
public class ChatVO {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(value = "聊天室編號")
	private Integer chatId;
	
	@ApiModelProperty(value = "活動細節編號")
	private Integer activityDetailId;

	public Integer getChatId() {
		return chatId;
	}

	public void setChatId(Integer chatId) {
		this.chatId = chatId;
	}

	public Integer getActivityDetailId() {
		return activityDetailId;
	}

	public void setActivityDetailId(Integer activityDetailId) {
		this.activityDetailId = activityDetailId;
	}
	
}
