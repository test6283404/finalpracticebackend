package com.martin.practice.module.chat.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.martin.practice.module.chat.bean.vo.ChatVO;
import com.martin.practice.module.chat.service.ChatService;
import com.martin.practice.module.member.bean.dto.ForIdDTO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 聊天模組的controller-聊天室
 */
@RestController
public class ChatController {

	@Autowired
	private ChatService chatService;

	Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * 透過activityDetailId尋找聊天室
	 * 
	 * @param activityDetailId
	 * @return ResponseEntity<ChatVO>
	 */
	@ApiOperation(value = "取得聊天室的資料")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/chat/findByActivityDetailId")
	public ResponseEntity<ChatVO> findById(
			@ApiParam(required = true, value = "請傳入活動細節編號") @RequestBody ForIdDTO activityDetailId) {
		log.info("用 取得聊天室 api ");

		try {
			ChatVO chatVO = chatService.findChatByActivityDetailId(activityDetailId.getId());

			if (chatVO != null) {
				log.info("取得成功");

				return new ResponseEntity<>(chatVO, HttpStatus.OK);
			} else {
				log.error("失敗");

				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	}

	/**
	 * 新增聊天室
	 * 
	 * @param ForIdDTO
	 * @return ResponseEntity<String>
	 */
	@ApiOperation(value = "新增聊天室")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/chat/create")
	public ResponseEntity<String> create(
			@ApiParam(required = true, value = "新增聊天室") @RequestBody ForIdDTO activityDetailId) {
		log.info("用 新增聊天室 api ");

		try {
			Integer create = chatService.createChat(activityDetailId.getId());

			if (create == 1) {
				log.info("新增聊天室成功");

				return new ResponseEntity<>("新增聊天室成功", HttpStatus.OK);
			} else {
				log.error("聊天室新增失敗");

				return new ResponseEntity<>("聊天室新增失敗", HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("聊天室新增失敗");

			return new ResponseEntity<>("聊天室新增失敗", HttpStatus.BAD_REQUEST);
		}
	}
}
