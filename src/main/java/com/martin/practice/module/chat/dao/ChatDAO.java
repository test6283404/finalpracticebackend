package com.martin.practice.module.chat.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.martin.practice.module.chat.bean.vo.ChatVO;

/**
 * 聊天室DAO
 */
@Mapper
public interface ChatDAO {
	
	/**
	 * 透過activityDetailId尋找chat
	 * @param activityDetailId
	 * @return ChatVO
	 */
	@Select("select * from chat where activityDetailId=#{activityDetailId}")
	ChatVO findChatByActivityDetailId(Integer activityDetailId);
	
	/**
	 * 新增chat
	 * @param chatVO
	 * @return Integer
	 */
	@Insert("insert into chat(activityDetailId) values (#{activityDetailId})")
	Integer createChat(Integer activityDetailId);
}
