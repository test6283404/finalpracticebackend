package com.martin.practice.module.chat.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.martin.practice.module.chat.bean.vo.ChatVO;
import com.martin.practice.module.chat.dao.ChatDAO;

/**
 * 聊天室Service
 */
@Service
public class ChatService {
	
	@Autowired
	private ChatDAO chatDAO;
	
	/**
	 * 透過activityDetailId尋找聊天室
	 * @param activityDetailId
	 * @return ChatVO
	 */
	public ChatVO findChatByActivityDetailId(Integer activityDetailId) {
		return chatDAO.findChatByActivityDetailId(activityDetailId);
	}
	
	/**
	 * 新增聊天室
	 * @param activityDetailId
	 * @return Integer
	 */
	public Integer createChat(Integer activityDetailId) {
		return chatDAO.createChat(activityDetailId);
	}
}
