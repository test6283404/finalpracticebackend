package com.martin.practice.module.chat.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.martin.practice.module.chat.bean.dto.CreateMessageDTO;
import com.martin.practice.module.chat.bean.vo.MessageVO;
import com.martin.practice.module.chat.service.MessageService;
import com.martin.practice.module.member.bean.dto.ForIdDTO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * 聊天模組的Controller-訊息
 */
@RestController
public class MessageController {
	
	@Autowired
	private MessageService messageService;
	
	Logger log = LoggerFactory.getLogger(getClass());
	
	/**
	 * 透過chatId尋找訊息
	 * 
	 * @param chatId
	 * @return ResponseEntity<List<MessageVO>>
	 */
	@ApiOperation(value = "透過聊天室編號尋找訊息")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/message/findByChatId")
	public ResponseEntity<List<MessageVO>> findByChatId(@ApiParam(required = true, value = "請傳入聊天室編號") @RequestBody ForIdDTO chatId) {
		log.info("用 透過聊天室編號尋找訊息 api ");

		try {
			List<MessageVO> messageList = messageService.findMessageByChatId(chatId.getId());

			if (!messageList.isEmpty()) {
				log.info("取得成功");

				return new ResponseEntity<>(messageList, HttpStatus.OK);
			} else {
				log.error("失敗");

				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 透過memberId尋找訊息
	 * 
	 * @param memberId
	 * @return ResponseEntity<List<MessageVO>>
	 */
	@ApiOperation(value = "透過會員編號尋找訊息")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/message/findBymemberId")
	public ResponseEntity<List<MessageVO>> findBymemberId(@ApiParam(required = true, value = "請傳入會員編號") @RequestBody ForIdDTO memberId) {
		log.info("用 透過會員編號尋找訊息 api ");

		try {
			List<MessageVO> messageList = messageService.findMessageByMemberId(memberId.getId());

			if (!messageList.isEmpty()) {
				log.info("取得成功");

				return new ResponseEntity<>(messageList, HttpStatus.OK);
			} else {
				log.error("失敗");

				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 新增訊息
	 * 
	 * @param CreateMessageDTO
	 * @return ResponseEntity<String>
	 */
	@ApiOperation(value = "新增訊息")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/message/create")
	public ResponseEntity<String> create(
			@ApiParam(required = true, value = "新增訊息") @RequestBody CreateMessageDTO createMessageDTO) {
		log.info("用 新增訊息 api ");

		try {
			if (createMessageDTO.getMessageContent() == null) {
				return new ResponseEntity<>("訊息新增失敗", HttpStatus.BAD_REQUEST);
			}
			
			Integer create = messageService.createMessage(createMessageDTO);

			if (create == 1) {
				log.info("新增訊息成功");

				return new ResponseEntity<>("新增訊息成功", HttpStatus.OK);
			} else {
				log.error("訊息新增失敗");

				return new ResponseEntity<>("訊息新增失敗", HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("訊息新增失敗");

			System.out.println(e);
			return new ResponseEntity<>("訊息新增失敗", HttpStatus.BAD_REQUEST);
		}
	}
}
