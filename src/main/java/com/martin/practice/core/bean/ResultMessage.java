package com.martin.practice.core.bean;

public class ResultMessage {
	private boolean isSystemMessage;

//	private Integer senderId;
	private String senderName;

	private Object messageContent;

	public ResultMessage() {
		super();
	}

	public ResultMessage(boolean isSystemMessage, String senderName, Object messageContent) {
		super();
		this.isSystemMessage = isSystemMessage;
		this.senderName = senderName;
		this.messageContent = messageContent;
	}

	public boolean isSystemMessage() {
		return isSystemMessage;
	}

	public void setSystemMessage(boolean isSystemMessage) {
		this.isSystemMessage = isSystemMessage;
	}

	public String getSenderId() {
		return senderName;
	}

	public void setSenderId(String senderName) {
		this.senderName = senderName;
	}

	public Object getMessageContent() {
		return messageContent;
	}

	public void setMessageContent(Object messageContent) {
		this.messageContent = messageContent;
	}
	
}
