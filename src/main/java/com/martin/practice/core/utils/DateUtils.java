package com.martin.practice.core.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class DateUtils {

	/**
	 * 取得時間並轉換成 yyyy/MM/dd hh:mm:ss 格式
	 * 
	 * @return String
	 */
	public static String generateDateAndTime() {
		Date date = new Date();

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

		return simpleDateFormat.format(date);
	}

	/**
	 * 取得字串並轉換成 yyyy-MM-dd Date格式
	 * 
	 * @return String
	 * @throws ParseException
	 */
	public static Date formatDateFromString(String date) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		return simpleDateFormat.parse(date);
	}

	/**
	 * 取得字串並轉換成 yyyy-MM-dd hh:mm:ss Date格式
	 * 
	 * @return String
	 * @throws ParseException
	 */
	public static Date formatDateTimeFromString(String dateTime) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

		return simpleDateFormat.parse(dateTime);
	}

	/**
	 * 將Date轉成String
	 * 
	 * @param dateTime
	 * @return
	 * @throws ParseException
	 */
	public static String transformDateToString(Date dateTime) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

		return dateFormat.format(dateTime);

	}

	public static boolean validateDate(Date datetime) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		String date = dateFormat.format(datetime);
		
		String regex = "^((19|20)[0-9]{2})-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$"
				+ "^((2000|2400|2800|(19|20)(0[48]|[2468][048]|[13579][26]))-02-29)$"
				+ "|^(((19|20)[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$"
				+ "|^(((19|20)[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$"
				+ "|^(((19|20)[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$";

		Pattern pattern = Pattern.compile(regex);

		return pattern.matcher(date).find();
	}
	
	public static boolean checkTimeResonable(Date startDate, Date endDate) {
		return startDate.before(endDate);
	}
}
