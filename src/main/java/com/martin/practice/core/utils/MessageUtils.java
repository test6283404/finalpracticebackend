package com.martin.practice.core.utils;

import com.alibaba.fastjson2.JSON;
import com.martin.practice.core.bean.ResultMessage;

public class MessageUtils {
	
	/**
	 * 物件變成json字串
	 * @param isSystemMessage
	 * @param senderName
	 * @param messageContent
	 * @return
	 */
	public static String getMessage(boolean isSystemMessage, String senderName, Object messageContent) {
        ResultMessage resultMessage = new ResultMessage();

        resultMessage.setSystemMessage(isSystemMessage);
        resultMessage.setMessageContent(messageContent);

        if (senderName != null) {
        	resultMessage.setSenderId(senderName);
        }

        return JSON.toJSONString(resultMessage);
    }
}
