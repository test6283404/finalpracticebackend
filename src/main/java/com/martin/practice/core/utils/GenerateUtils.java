package com.martin.practice.core.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GenerateUtils {

	/**
	 * 生成隨機密碼
	 * @return
	 */
	public static String getRandomPassword() {
		String number = "0123456789";
		String capital = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String character = "abcdefghijklmnopqrstuvwxyz";
		String special = "@#$%";

		List<Character> list = new ArrayList<>();
		
		list.add(number.charAt((int) (Math.random() * 10)));
		list.add(number.charAt((int) (Math.random() * 10)));
		list.add(number.charAt((int) (Math.random() * 10)));
		list.add(number.charAt((int) (Math.random() * 10)));
		list.add(capital.charAt((int) (Math.random() * 26)));
		list.add(capital.charAt((int) (Math.random() * 26)));
		list.add(character.charAt((int) (Math.random() * 26)));
		list.add(special.charAt((int) (Math.random() * 4)));
		
		Collections.shuffle(list);
		
		String randomPassword = "";
		
		for (int i = 0; i < list.size(); i++) {
			randomPassword += list.get(i);
		}
		
		return randomPassword;
	}
}
