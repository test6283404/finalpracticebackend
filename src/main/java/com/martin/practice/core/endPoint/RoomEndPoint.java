package com.martin.practice.core.endPoint;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpSession;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson2.JSON;
import com.martin.practice.core.config.GetHttpSessionConfig;
import com.martin.practice.core.utils.MessageUtils;
import com.martin.practice.module.chat.bean.dto.CreateMessageDTO;
import com.martin.practice.module.member.bean.dto.LoginMemberDTO;

/**
 * WebSocket端點
 */
@ServerEndpoint(value = "/chatRoom", configurator = GetHttpSessionConfig.class)
@Component
public class RoomEndPoint {
	private static final Map<Integer, Session> onlineUsers = new ConcurrentHashMap<>();

    private HttpSession httpSession;
    
    Logger log = LoggerFactory.getLogger(getClass());

    /**
     * WebSocket初始
     * @param session
     * @param config
     */
    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
    	log.info("websocket 打開");
    	
        this.httpSession = (HttpSession) config.getUserProperties().get(HttpSession.class.getName());

        LoginMemberDTO loginMemberDTO = (LoginMemberDTO) this.httpSession.getAttribute("loginMember");
        Integer memberId = loginMemberDTO.getMemberId();
        String memberName = loginMemberDTO.getMemberName();

        onlineUsers.put(memberId, session);

        String message = MessageUtils.getMessage(true, null, getFriends());

        broadcastAllUsers(message);
    }

    /**
     * 連接中
     * @param message
     * @throws IOException
     */
    @OnMessage
    public void onMessage(String message) throws IOException {
    	log.info("websocket 連接");
    	
    	CreateMessageDTO createMessageDTO = JSON.parseObject(message, CreateMessageDTO.class);

        Integer memberId = createMessageDTO.getMemberId();
        String messageContent = createMessageDTO.getMessageContent();

        Session session = onlineUsers.get(memberId);

        if (session != null && session.isOpen()) {
        	LoginMemberDTO loginMemberDTO = (LoginMemberDTO) this.httpSession.getAttribute("loginMember");
            Integer id = loginMemberDTO.getMemberId();
            String senderName = loginMemberDTO.getMemberName();

            String transferMessage = MessageUtils.getMessage(false, senderName, messageContent);
            
            broadcastAllUsers(transferMessage);
        }
    }

    /**
     * WebSocket關閉
     * @param session
     */
    @OnClose
    public void onClose(Session session) {
    	log.info("websocket 關閉");
    	
    	LoginMemberDTO loginMemberDTO = (LoginMemberDTO) this.httpSession.getAttribute("loginMember");
        Integer memberId = loginMemberDTO.getMemberId();

        onlineUsers.remove(memberId);

        String message = MessageUtils.getMessage(true, null, getFriends());

        broadcastAllUsers(message);
    }

    /**
     * WebSocket錯誤
     * @param session
     * @param throwable
     */
    @OnError
    public void onError(Session session, Throwable throwable) {
        System.out.println("WebSocket Error: " + throwable.getMessage());

        try {
            session.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 傳送訊息
     * @param message
     */
    private void broadcastAllUsers(String message) {
        Set<Map.Entry<Integer, Session>> entries = onlineUsers.entrySet();

        for (Map.Entry<Integer, Session> entry :
                entries) {
            Session session = entry.getValue();

            try {
                session.getBasicRemote().sendText(message);
                
            } catch (Exception ignored) {

            }
        }
    }

    /**
     * 取得所有會員
     * @return
     */
    public Set<Integer> getFriends() {
        return onlineUsers.keySet();
    }
}
