package com.martin.practice.core.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import dev.samstevens.totp.qr.QrGenerator;
import dev.samstevens.totp.qr.ZxingPngQrGenerator;
import dev.samstevens.totp.secret.DefaultSecretGenerator;
import dev.samstevens.totp.secret.SecretGenerator;

/**
 * 註冊TOTP
 */
@Configuration
public class TOTPConfig {

    @Bean
    SecretGenerator secretGenerator() {
		return new DefaultSecretGenerator();
	}

    @Bean
    QrGenerator qrGenerator() {
		return new ZxingPngQrGenerator();
	}
	
}
