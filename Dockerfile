FROM adoptopenjdk/openjdk11
COPY ./target/java-docker-practice-backend-0.0.1-SNAPSHOT.jar java-docker-backend.jar
EXPOSE 8091
ENTRYPOINT ["java","-jar","java-docker-backend.jar"]
