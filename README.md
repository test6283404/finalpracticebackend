程式的使用說明書-後端
===

程式概述
---
會員系統、活動系統、聊天系統、多驗證系統

程式結構說明
---
**會員系統**: 
1. 註冊功能
2. 登入
3. 管理
4. 登出

**活動系統:** 
1. 管理功能

**聊天系統:** 
1. 討論功能

**多驗證系統:** 
1. 驗證功能

操作說明
---

**自行建置jar檔**

PowerShell 執行

   1. 先cd到專案所在目錄
      1. mvn compile
      2. mvn package
   2. 再cd到專案中的子目錄 '<你的專案位置>\target'
      1. java -jar java-docker-practice-backend-0.0.1-SNAPSHOT.jar

注意事項
---
JDK 版本配置

`java version "11"`

SpringBoot 版本配置

`SpringBoot 2.5.6`


## File Structure

```sh
─src
│  ├─main
│  │  ├─java
│  │  │  └─com
│  │  │      └─martin
│  │  │          └─practice
│  │  │              │  JavaPracticeApplication.java
│  │  │              │
│  │  │              ├─core
│  │  │              │  ├─bean
│  │  │              │  │      ResultMessage.java
│  │  │              │  │
│  │  │              │  ├─config
│  │  │              │  │      CorsConfigFilter.java
│  │  │              │  │      GetHttpSessionConfig.java
│  │  │              │  │      LoginConfig.java
│  │  │              │  │      SecurityConfig.java
│  │  │              │  │      SpringFoxConfig.java
│  │  │              │  │      TOTPConfig.java
│  │  │              │  │      WebSocketConfig.java
│  │  │              │  │
│  │  │              │  ├─controller
│  │  │              │  │      GoogleAuthController.java
│  │  │              │  │
│  │  │              │  ├─endPoint
│  │  │              │  │      RoomEndPoint.java
│  │  │              │  │
│  │  │              │  └─utils
│  │  │              │          DateUtils.java
│  │  │              │          MessageUtils.java
│  │  │              │
│  │  │              └─module
│  │  │                  ├─activity
│  │  │                  │  ├─bean
│  │  │                  │  │  ├─dto
│  │  │                  │  │  │      CreateActivityDetailDTO.java
│  │  │                  │  │  │      CreateActivityDTO.java
│  │  │                  │  │  │      CreateDiscussDTO.java
│  │  │                  │  │  │      CreateGroupDTO.java
│  │  │                  │  │  │      UpdateActivityDetailDTO.java
│  │  │                  │  │  │      UpdateActivityDTO.java
│  │  │                  │  │  │
│  │  │                  │  │  └─vo
│  │  │                  │  │          ActivityDetailVO.java
│  │  │                  │  │          ActivityVO.java
│  │  │                  │  │          DiscussVO.java
│  │  │                  │  │          GroupVO.java
│  │  │                  │  │
│  │  │                  │  ├─controller
│  │  │                  │  │      ActivityController.java
│  │  │                  │  │      ActivityDetailController.java
│  │  │                  │  │      DiscussController.java
│  │  │                  │  │      GroupController.java
│  │  │                  │  │
│  │  │                  │  ├─dao
│  │  │                  │  │      ActivityDAO.java
│  │  │                  │  │      ActivityDetailDAO.java
│  │  │                  │  │      DiscussDAO.java
│  │  │                  │  │      GroupDAO.java
│  │  │                  │  │
│  │  │                  │  └─service
│  │  │                  │          ActivityDetailService.java
│  │  │                  │          ActivityService.java
│  │  │                  │          DiscussService.java
│  │  │                  │          GroupService.java
│  │  │                  │
│  │  │                  ├─chat
│  │  │                  │  ├─bean
│  │  │                  │  │  ├─dto
│  │  │                  │  │  │      CreateMessageDTO.java
│  │  │                  │  │  │
│  │  │                  │  │  └─vo
│  │  │                  │  │          ChatVO.java
│  │  │                  │  │          MessageVO.java
│  │  │                  │  │
│  │  │                  │  ├─controller
│  │  │                  │  │      ChatController.java
│  │  │                  │  │      MessageController.java
│  │  │                  │  │
│  │  │                  │  ├─dao
│  │  │                  │  │      ChatDAO.java
│  │  │                  │  │      MessageDAO.java
│  │  │                  │  │
│  │  │                  │  └─service
│  │  │                  │          ChatService.java
│  │  │                  │          MessageService.java
│  │  │                  │
│  │  │                  ├─member
│  │  │                  │  ├─bean
│  │  │                  │  │  ├─dto
│  │  │                  │  │  │      AccountAndPasswordDTO.java
│  │  │                  │  │  │      CreateMemberDetailDTO.java
│  │  │                  │  │  │      CreateMemberDTO.java
│  │  │                  │  │  │      CreateRoleDTO.java
│  │  │                  │  │  │      ForCreateAndUpdateDTO.java
│  │  │                  │  │  │      ForIdDTO.java
│  │  │                  │  │  │      InputNameDTO.java
│  │  │                  │  │  │      LoginMemberDTO.java
│  │  │                  │  │  │      LoginUserDTO.java
│  │  │                  │  │  │      UpdateMemberDTO.java
│  │  │                  │  │  │      UsernameAndPasswordDTO.java
│  │  │                  │  │  │
│  │  │                  │  │  └─vo
│  │  │                  │  │          MemberVO.java
│  │  │                  │  │          RoleVO.java
│  │  │                  │  │
│  │  │                  │  ├─controller
│  │  │                  │  │      MemberController.java
│  │  │                  │  │      RoleController.java
│  │  │                  │  │
│  │  │                  │  ├─dao
│  │  │                  │  │      MemberDAO.java
│  │  │                  │  │      RoleDAO.java
│  │  │                  │  │
│  │  │                  │  ├─mapper
│  │  │                  │  └─service
│  │  │                  │          MemberService.java
│  │  │                  │          RoleService.java
│  │  │                  │
│  │  │                  └─mfa
│  │  │                      ├─bean
│  │  │                      │  ├─dto
│  │  │                      │  │      VerifyMemberDTO.java
│  │  │                      │  │
│  │  │                      │  └─vo
│  │  │                      │          MemberDetailVO.java
│  │  │                      │
│  │  │                      ├─controller
│  │  │                      │      MemberDetailController.java
│  │  │                      │
│  │  │                      ├─dao
│  │  │                      │      MemberDetailDAO.java
│  │  │                      │
│  │  │                      └─service
│  │  │                              MemberDetailService.java
│  │  │
│  │  └─resources
│  │      │  application.yaml
│  │      │
```